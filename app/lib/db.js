
var collectionName = 'Projects';
var moment = require('alloy/moment');
if (Alloy.Globals.verboseLog) { Ti.API.info("db entry "); };


function setupSections(delAll){
	if (delAll){
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Deleting All Sections Records"); };
		var collection =  Alloy.createCollection('Sections');	
		collection.fetch();
		Ti.API.info("Deleting Sections: " + collection.length);			
		var sql = "DELETE FROM " + collection.config.adapter.collection_name;
		db = Ti.Database.open(collection.config.adapter.db_name);
		db.execute(sql);
		db.close();
		collection.trigger('sync');	
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Deleting All Conditions Records"); };
		collection =  Alloy.createCollection('Conditions');	
		collection.fetch();
		if (Alloy.Globals.verboseLog) {Ti.API.info("Deleting Conditions: " + collection.length);};			
		var sql = "DELETE FROM " + collection.config.adapter.collection_name;
		db = Ti.Database.open(collection.config.adapter.db_name);
		db.execute(sql);
		db.close();
		collection.trigger('sync');	
	}
	newSection('OBJECTIVES & PRIORITY','A');
	newSection('BENCHMARKING','B');
	newSection('PROJECT SCOPE','C');
	newSection('PROJECT SITE','D');
	newSection('STAKEHOLDER & COMMUNITY RELATIONS','E');
	newSection('PROJECT CONTRACT','F');
	newSection('PROCUREMENT','G');
	newSection('PROJECT ORGANIZATION','H');
	newSection('PROJECT COMMUNICATIONS','I');
	newSection('HEALTH, SAFETY, & ENVIRONMENT','J');
	newSection('LOGISTICS','K');
	newSection('TEMPORARY FACILITIES','L');
	newSection('ONSITE TEMPORARY POWER','M');
	newSection('CONSTRUCTION & DEMOLITION WASTE MANAGEMENT','N');
	newSection('CRAFT LABOR','O');
	newSection('CONSTRUCTION EQUIPMENT','P');
	newSection('COMMISSIONING','Q');
	newConditions();	
}

function dropAllTables(){
	if (Alloy.Globals.verboseLog) {Ti.API.info("DB: dropping all tables");};
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	db.execute('DROP TABLE IF EXISTS Conditionsscore');
	db.execute('DROP TABLE IF EXISTS Conditions');
	db.execute('DROP TABLE IF EXISTS Impactscore');
	db.execute('DROP TABLE IF EXISTS Laverage');//to be removed before deployment - mistake
	db.execute('DROP TABLE IF EXISTS Leverage');
	db.execute('DROP TABLE IF EXISTS Projects');
	db.execute('DROP TABLE IF EXISTS Sections');
	db.execute('DROP TABLE IF EXISTS Settings');
	db.execute('DROP TABLE IF EXISTS Saprimary');
	db.execute('DROP TABLE IF EXISTS Results'); //to be removed before deployment - mistake
	db.execute('DROP TABLE IF EXISTS Saresults');
	db.close();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("All tables dropped.");};	
}

function getCurrentVersion() {
	
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	db.execute('CREATE TABLE IF NOT EXISTS Settings (Item TEXT, Value TEXT)');
	db.close();	
	var settings = Alloy.createCollection('Settings');
	
	settings.fetch();
	if (Alloy.Globals.verboseLog) {Ti.API.info("Settings: " + JSON.stringify(settings));};
	if (JSON.stringify(settings).length > 2){
		settings = settings.where({Item:"VERSION"})[0];
		if (Alloy.Globals.verboseLog) {Ti.API.info("Settings: " + JSON.stringify(settings).length);};		
		settings = settings.toJSON();
		if (settings.Item){
			return settings.Value;	
		} else {
			return "";
		}	
	} else {
		return "";
	}
}

function saveDBVersion(version){
	// var row = Alloy.createModel('Settings', {
		// Item: "VERSION",
        // Value: version				
	// });
	// if (Alloy.Globals.verboseLog) {Ti.API.info("Settings for version: " + JSON.stringify(row));};
	// row.save();	
	
	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
	db.execute('CREATE TABLE IF NOT EXISTS Settings (Item TEXT, Value TEXT)');
	db.execute('CREATE TABLE IF NOT EXISTS Saresults (ResultsGUID TEXT NOT NULL PRIMARY KEY,ProjGUID TEXT,MyRank INTEGER,Rank INTEGER,SA TEXT,SASort INTEGER,Title TEXT,Link TEXT,RI TEXT,RISort INTEGER)');
	db.execute('INSERT INTO Settings (Item, Value) VALUES (?,?)', "VERSION", version);
	db.close();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Settings Record Saved");};	
}

function updateMyRankFlag(ProjGuid, flag) {
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Updating MyRankFlag status"); };
			var projects =  Alloy.createCollection(collectionName);
			projects.fetch();
			projects = projects.where({ProjGUID:ProjGuid})[0];
			if (flag){
				var status='YES';	
			} else {
				var status='NO';
			}			
			projects.save({
						UseMyRank:status
					});
			if (Alloy.Globals.verboseLog) {Ti.API.info("Record Saved MyRankFlag: " + ProjGuid);};	
}

function newSection(name, id){
	var row = Alloy.createModel('Sections', {
		// id: auto incremented primary key
		SectionGUID: Ti.Platform.createUUID(),    
		SectionName: name,
        SectionID: id				
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("Section: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Section Record Saved");};	
}

function newCondition(sectionId, id, name){
	var row = Alloy.createModel('Conditions', {
		// id: auto incremented primary key
		ConditionGUID: Ti.Platform.createUUID(),    
		ConditionName: name,
        ConditionID: id,
        SectionID : sectionId				
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("Condition: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Condition Record Saved");};	
}

function newImpactScore(symbol, value){
	var row = Alloy.createModel('Impactscore', {
		// id: auto incremented primary key
		ScoreGUID: Ti.Platform.createUUID(),    
		Symbol: symbol,
        BaseValue: value			
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("ImpactScore: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("ImpactScore Record Saved");};	
}

function newConditionScore(count, score){
	var row = Alloy.createModel('Conditionsscore', {
		// id: auto incremented primary key
		CondScoreGUID: Ti.Platform.createUUID(),    
		Count: count,
        Score: score			
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("ConditionScore: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("ConditionScore Record Saved");};	
}

function deleteProjectResults(projGuid)	{  
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Deleting Project Results" + projGuid); };
	if (projGuid.length > 0) {
		var collection =  Alloy.createCollection('Saresults');	
		collection.fetch();
		var sql = "DELETE FROM " + collection.config.adapter.collection_name + " where ProjGUID='" + projGuid + "'";
		db = Ti.Database.open(collection.config.adapter.db_name);
		db.execute(sql);
		db.close();
		collection.trigger('sync');
		return true;
	} else {
		return false;
	}
}

function setupImpactScores(){
	newImpactScore('N','0');
	newImpactScore('+','0.6');
	newImpactScore('++','1');
	newImpactScore('-','-0.6');
	newImpactScore('--','-1');
}

function setupConditionScores(){
	newConditionScore('0','0.1');
	newConditionScore('1','0.33');
	newConditionScore('2','0.67');
	newConditionScore('3','1');
}

function setupLeverages(){
	newLeverage('1','Project Organization','The project team is a collaborative and communicative organization.','H1');
	newLeverage('1','Project Organization','Project management has taken a lead role in endorsing sustainable solutions.','H3');
	newLeverage('1','Project Organization','The project team has experience incorporating sustainability provisions.','H8');
	newLeverage('2','Project Scope','The project is large and complex.','C8');
	newLeverage('2','Stakeholder & Community Relations','Community members have access to the Internet.','E1');
	newLeverage('2','Stakeholder & Community Relations','Project stakeholders and local community leaders are clearly defined and accessible.','E2');
	newLeverage('3','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('3','Project Organization','The project organization and/or the sustainability program are large in size, scope, and/or effort.','H4');
	newLeverage('3','Project Organization','The team’s sustainability effort is new, fledgling, or ill-structured.','H5');
	newLeverage('4','Project Organization','Project management has taken a lead role in endorsing sustainable solutions.','H3');
	newLeverage('4','Project Organization','The project team has experience incorporating sustainability provisions.','H8');
	newLeverage('4','Project Scope','The project is large and complex.','C8');
	newLeverage('5','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('5','Project Scope','The project is large and complex.','C8');
	newLeverage('5','Stakeholder & Community Relations','The project owner, stakeholders, and/or local community have diverse interests relative to sustainability.','E3');
	newLeverage('6','Project Scope','The project is large and complex.','C8');
	newLeverage('6','Stakeholder & Community Relations','Project stakeholders and local community leaders are clearly defined and accessible.','E2');
	newLeverage('6','Stakeholder & Community Relations','The project owner, stakeholders, and/or local community have diverse interests relative to sustainability.','E3');
	newLeverage('7','Objectives & Priority','Sufficient resources are available to modify schedules.','A2');
	newLeverage('7','Objectives & Priority','The project schedule and budget are flexible.','A4');
	newLeverage('7','Project Site','There is sufficient infrastructure to minimize traffic into and out of the site.','D10');
	newLeverage('8','Objectives & Priority','Sufficient resources are available to modify schedules.','A2');
	newLeverage('8','Objectives & Priority','The project schedule and budget are flexible.','A4');
	newLeverage('8','On-Site Temporary Power','There are several options for providing electricity.','M5');
	newLeverage('9','Project Communications','All parties are willing to use electronic communications and align on same electronic systems.','I1');
	newLeverage('9','Project Communications','Electronic programs / forms are available and individuals with expertise are available to run them.','I2');
	newLeverage('9','Project Communications','All project parties have computers or tablets and knowledge of electronic systems.','I3');
	newLeverage('10','Benchmarking','Sustainability performance and resource data are available.','B2');
	newLeverage('10','Benchmarking','The project team is interested in evaluating and improving sustainability performance.','B3');
	newLeverage('10','Project Contract','The project team is interested in including, or has already incorporated, sustainability requirements into the prime contract.','F5');
	newLeverage('11','Procurement','The project has a few large vendors and suppliers.','G9');
	newLeverage('11','Procurement','The project has a significant number of suppliers and vendors who have certifications or could obtain certifications.','G10');
	newLeverage('11','Procurement','The project has mature suppliers and vendors.','G11');
	newLeverage('12','Project Contract','Alternative project delivery methods are available for the project.','F1');
	newLeverage('12','Project Contract','The project team is interested in including, or has already incorporated, sustainability requirements into the prime contract.','F5');
	newLeverage('12','Project Organization','The owner and contractor agree to share the benefits/savings from employing sustainable solutions.','H7');
	newLeverage('13','Craft Labor','A sufficient number of contractors are available.','O5');
	newLeverage('13','Project Scope','The project is large and complex.','C8');
	newLeverage('13','Stakeholder & Community Relations','The project owner, stakeholders, and/or local community have diverse interests relative to sustainability.','E3');
	newLeverage('14','Procurement','The project for national companies that specify goals for local content.','G5');
	newLeverage('14','Procurement','Work that can be identified to go to SBEs / WBEs / MBEs doesn’t require specialized expertise that might not be locally available.','G15');
	newLeverage('14','Project Scope','The project is large and complex.','C8');
	newLeverage('15','Project Contract','The project team is interested in including, or has already incorporated, sustainability requirements into the prime contract.','F5');
	newLeverage('15','Project Organization','The owner and contractor agree to share the benefits/savings from employing sustainable solutions.','H7');
	newLeverage('15','Project Scope','The project is large and complex.','C8');
	newLeverage('16','Craft Labor','The project is suffering from low field craft productivity.','O6');
	newLeverage('16','Procurement','The project has high local content requirements for materials and services.','G12');
	newLeverage('16','Project Scope','Selection of construction methods involves many complex tradeoffs.','C5');
	newLeverage('17','Health, Safety, & Environment','Regional labor safety performance is below expectations.','J7');
	newLeverage('17','Project Site','The project site is small in size or congested.','D9');
	newLeverage('17','Temporary Facilities','A stick-built approach would involve a significant amount of scaffolding.','L1');
	newLeverage('18','Health, Safety, & Environment','The project is located in an area with recognized air quality problems.','J10');
	newLeverage('18','Project Site','Project will generate a significant amount of transport traffic.','D6');
	newLeverage('18','Project Site','The project is located in an area with significant traffic congestion.','D8');
	newLeverage('19','Construction Equipment','Alternative construction equipment is available in sizes sufficient to support construction activities.','P1');
	newLeverage('19','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('19','Project Organization','The contractor has experience implementing sustainable solutions/practices.','H6');
	newLeverage('20','Project Contract','Project contract provides an incentive for contractor-proposed cost-reducing changes (e.g., as a Value Engineering Change clause).','F2');
	newLeverage('20','Project Contract','Project contract provides some flexibility for contractor material substitutions.','F3');
	newLeverage('20','Project Organization','The contractor has experience implementing sustainable solutions/practices.','H6');
	newLeverage('21','Health, Safety, & Environment','Construction activities will cause a significant amount of noise and/or vibration for a lengthy duration of time.','J2');
	newLeverage('21','Health, Safety, & Environment','Many neighbors adjacent to the jobsite are sensitive to the noise and/or vibration caused by the project.','J5');
	newLeverage('21','Health, Safety, & Environment','The project involves a significant amount of pile-driving, rock hammering, or blasting.','J9');
	newLeverage('22','C&D Waste Management','Local recycling infrastructure is in place.','N1');
	newLeverage('22','Objectives & Priority','Project schedule allows time for selective demolition activity.','A1');
	newLeverage('22','Project Scope','The project involves a significant amount of demolition.','C7');
	newLeverage('23','Health, Safety, & Environment','Adjacent project neighbors are very sensitive to project-generated noise, dust, and/or equipment exhaust.','J1');
	newLeverage('23','Health, Safety, & Environment','The project is located in an area with recognized air quality problems.','J10');
	newLeverage('23','Project Scope','Project execution involves large-scale earthwork and grading operations.','C2');
	newLeverage('24','Construction Equipment','The project involves a substantial amount of dunnage for temporary support of construction equipment (such as cranes).','P6');
	newLeverage('24','Construction Equipment','The project involves extensive use of non-mobile heavy cranes.','P8');
	newLeverage('24','Project Site','Project site is large in size.','D5');
	newLeverage('25','Temporary Facilities','Future projects by this contractor will very likely entail shoring, formwork, and/or scaffolding.','L2');
	newLeverage('25','Temporary Facilities','Project design entails a substantial amount of repetition or modularity, thereby leveraging standardization of shoring, formwork, and/or scaffolding.','L4');
	newLeverage('25','Temporary Facilities','Project involves a substantial amount of shoring, formwork, and/or scaffolding.','L5');
	newLeverage('26','Objectives & Priority','The project is schedule-critical.','A3');
	newLeverage('26','Project Site','Project region has a significant archeological history.','D3');
	newLeverage('26','Project Site','Project region has some endangered species.','D4');
	newLeverage('27','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('27','Project Scope','Project site includes existing trees and vegetation to be protected.','C4');
	newLeverage('27','Project Scope','Site congestion could result in damage to existing trees/vegetation.','C6');
	newLeverage('28','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('28','Project Scope','The project is large and complex.','C8');
	newLeverage('28','Temporary Facilities','Project involves a worker camp.','L6');
	newLeverage('29','Craft Labor','Local labor supply is extremely limited.','O1');
	newLeverage('29','Craft Labor','Project size is very large relative to the local labor supply.','O3');
	newLeverage('29','Craft Labor','The project will have a culturally diverse workforce.','O8');
	newLeverage('30','On-Site Temporary Power','Project execution requires a significant amount of electrical power.','M2');
	newLeverage('30','On-Site Temporary Power','Project is very distant from the local power grid.','M3');
	newLeverage('30','On-Site Temporary Power','Regional energy costs are high and/or the local grid-sourced power has significant negative environmental impacts.','M4');
	newLeverage('31','On-Site Temporary Power','An energy management system has not yet been implemented on the project.','M1');
	newLeverage('31','On-Site Temporary Power','Regional energy costs are high and/or the local grid-sourced power has significant negative environmental impacts.','M4');
	newLeverage('31','On-Site Temporary Power','Workplace culture has not yet focused on energy efficiency.','M6');
	newLeverage('32','Project Site','Local solar conditions are conducive to operation of facility solar-support systems.','D1');
	newLeverage('32','Temporary Facilities','Investment into reusable modular autonomous facility units may be spread over several projects.','L3');
	newLeverage('32','Temporary Facilities','Such units are locally available for rent/lease.','L7');
	newLeverage('33','Health, Safety, & Environment','Project involves buildings with human occupants.','J6');
	newLeverage('33','Project Organization','Project management has taken a lead role in endorsing sustainable solutions.','H3');
	newLeverage('33','Project Scope','Building HVAC systems are installed and operational early in construction.','C1');
	newLeverage('34','C&D Waste Management','Local regulatory restrictions enable the use of grey water.','N2');
	newLeverage('34','C&D Waste Management','Project construction processes can generate a significant amount of grey water.','N5');
	newLeverage('34','Project Site','Project is located in an area where water is scarce.','D2');
	newLeverage('35','Health, Safety, & Environment','Adjacent project neighbors are very sensitive to project-generated noise, dust, and/or equipment exhaust.','J1');
	newLeverage('35','Health, Safety, & Environment','Many community residents live adjacent to the project site.','J4');
	newLeverage('35','Health, Safety, & Environment','The local community (and especially project neighbors) are not supportive of the project.','J8');
	newLeverage('36','C&D Waste Management','Local recycling infrastructure is in place.','N1');
	newLeverage('36','C&D Waste Management','Regional landfill dumping fees are relatively high.','N8');
	newLeverage('36','Project Scope','The project involves a significant amount of demolition.','C7');
	newLeverage('37','C&D Waste Management','Local recycling infrastructure is in place.','N1');
	newLeverage('37','C&D Waste Management','Other projects in the region can benefit from reuse of waste from this project.','N4');
	newLeverage('37','Project Scope','The project involves a significant amount of demolition.','C7');
	newLeverage('38','Craft Labor','Many capital projects in the area compete for skilled labor.','O2');
	newLeverage('38','Craft Labor','Project will draw from a migrant labor pool.','O4');
	newLeverage('38','Craft Labor','The region has a shortage of skilled craft labor.','O9');
	newLeverage('39','Craft Labor','The tradeoffs associated with the different sources of employment are not obvious.','O10');
	newLeverage('39','Procurement','The project has high local content requirements for materials and services.','G12');
	newLeverage('39','Stakeholder & Community Relations','Project stakeholders and local community leaders are clearly defined and accessible.','E2');
	newLeverage('40','Craft Labor','The project will have a culturally diverse workforce.','O8');
	newLeverage('40','Craft Labor','The region has a shortage of skilled craft labor.','O9');
	newLeverage('40','Project Scope','The project is large and complex.','C8');
	newLeverage('41','Craft Labor','The project region offers competitive sources for goods and services.','O7');
	newLeverage('41','Procurement','International alliance-type sourcing is the contractor’s standard (default) approach.','G2');
	newLeverage('41','Procurement','The project has high local content requirements for materials and services.','G12');
	newLeverage('42','C&D Waste Management','Recycling of materials is already part of the project team’s culture.','N7');
	newLeverage('42','Procurement','Current methods of packaging/packing material result in excess waste.','G1');
	newLeverage('42','Procurement','The owner and/or contractor have large market share and can influence shipping methods of vendors/suppliers.','G4');
	newLeverage('43','Craft Labor','The project is suffering from low field craft productivity.','O6');
	newLeverage('43','Project Scope','The project is large and complex.','C8');
	newLeverage('43','Project Site','The project site is small in size or congested.','D9');
	newLeverage('44','C&D Waste Management','Project waste management efforts have been minimal.','N6');
	newLeverage('44','Procurement','The project involves a significant amount of consumable materials to support construction processes.','G6');
	newLeverage('44','Project Scope','Project fabrication and/or construction processes involve advanced technologies.','C3');
	newLeverage('45','Procurement','The project owner is not attentive to contractor procurement approaches.','G8');
	newLeverage('45','Procurement','The project team is interested in improving the accuracy of quantity take-off estimates.','G13');
	newLeverage('45','Project Contract','The construction contract is a cost-reimbursable type.','F4');
	newLeverage('46','Procurement','Many material items are purchased in large bulk quantities.','G3');
	newLeverage('46','Procurement','The project team is interested in improving the accuracy of quantity take-off estimates.','G13');
	newLeverage('46','Project Scope','The project involves a significant amount of demolition.','C7');
	newLeverage('47','Construction Equipment','The contractor’s existing equipment fleet includes many old pieces that are not fuel-efficient.','P5');
	newLeverage('47','Construction Equipment','The project involves a substantial amount of heavy construction equipment.','P7');
	newLeverage('47','Health, Safety, & Environment','The project is located in an area with recognized air quality problems.','J10');
	newLeverage('48','Construction Equipment','Construction equipment capacity is often much higher than needed for the task.','P2');
	newLeverage('48','Construction Equipment','Construction equipment fleet managers and operators are mostly unaware of environmental effect of equipment operations.','P3');
	newLeverage('48','Construction Equipment','Construction equipment selection decisions are most often driven by convenient availability.','P4');
	newLeverage('49','Procurement','The project involves many small, uncoordinated deliveries.','G7');
	newLeverage('49','Procurement','Transport equipment tends to be oversized relative to needs.','G14');
	newLeverage('49','Project Site','The project is located in an area with significant traffic congestion.','D8');
	newLeverage('50','Logistics','The project has a complex logistics program.','K1');
	newLeverage('50','Objectives & Priority','The project schedule and budget are flexible.','A4');
	newLeverage('50','Project Site','Projects in non-arctic or non-desert environments.','D7');
	newLeverage('51','Construction Equipment','The project involves a substantial amount of heavy construction equipment.','P7');
	newLeverage('51','Project Organization','Contractors and subcontractors have equipment maintenance programs in place.','H2');
	newLeverage('51','Project Site','The project site is small in size or congested.','D9');
	newLeverage('52','Health, Safety, & Environment','Local jurisdiction requires clean-up of any materials placed on roadway.','J3');
	newLeverage('52','Project Scope','Project execution involves large-scale earthwork and grading operations.','C2');
	newLeverage('52','Project Site','The project is located in an area with significant traffic congestion.','D8');
	newLeverage('53','C&D Waste Management','Manufactured goods that are rejected product can have alternative uses.','N3');
	newLeverage('53','Commissioning','The commissioning team is familiar with sustainability concepts.','Q1');
	newLeverage('53','Commissioning','The owner and general contractor are interested in optimizing facility commissioning to improve the overall quality and performance of the final product.','Q2');
	newLeverage('54','Benchmarking','Significant sustainability activities occurred on the project.','B1');
	newLeverage('54','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area.','J11');
	newLeverage('54','Project Organization','Project management has taken a lead role in endorsing sustainable solutions.','H3');
	newLeverage('55','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('55','Project Scope','Project execution involves large scale earthwork and grading operations','C2');
	newLeverage('55','Project Scope','Selection of construction methods involves many complex tradeoffs','C5');
	newLeverage('56','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('56','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area','J11');
	newLeverage('56','Project Scope','Project execution involves large-scale earthwork and grading operations','C2');
	newLeverage('57','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('57','Health, Safety, & Environment','The project is located in an environmentally/socially-sensitive area','J11');
	newLeverage('57','Health, Safety, & Environment','The project is located in an area with recognized air quality problems','J10');
	newLeverage('58','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('58','Project Scope','The project scope includes the construction of new refinery units and support infrastructure','C10');
	newLeverage('58','Project Scope','Project fabrication and/or construction processes involve advance technologies','C3');
	newLeverage('59','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('59','Health, Safety, & Environment','Project involves buildings with human occupants','J6');
	newLeverage('59','Project Organization','Project Management has taken a lead role in endorsing sustainable solutions','H3');
	newLeverage('60','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('60','Health, Safety, & Environment','Project involves buildings with human occupants','J6');
	newLeverage('60','Project Organization','The project team has experience incorporating sustainability provisions','H8');
	newLeverage('61','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('61','Temporary Facilities','Project design entails a substantial amount of repetition or modularity, thereby leveraging standardization of shoring, formwork, and/or scaffolding','L4');
	newLeverage('61','Commissioning','Commissioning team is familiar with sustainability concepts','Q1');
	newLeverage('62','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('62','Procurement','Project involves a significant amount of consumable materials to support construction processes','G6');
	newLeverage('62','Temporary Facilities','Project design entails a substantial amount of repetition or modularity, thereby leveraging standardization of shoring, formwork, and /or scaffolding','L4');
	newLeverage('63','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('63','Project Site','The project site is small in size or congested','D9');
	newLeverage('63','Stakeholder & Community Relations','The owner, stakeholders, and/or local community have diverse interests relative to sustainability','E3');
	newLeverage('64','Project Scope','The project is currently in feasibility or early design phase','C9');
	newLeverage('64','Project Organization','The contractor has experience implementing sustainability solutions/practices','H6');
	newLeverage('64','Project Scope','Selection of construction methods involves many complex trade-offs','C5');
	newLeverage('65','Benchmarking','Sustainability performance and resource data are available','B2');
	newLeverage('65','Project Organization','Project management has taken a lead role in endorsing sustainable solutions','H3');
	newLeverage('65','Project Organization','Project organization and /or sustainability program are large in size, scope, and/or effort','H4');
	newLeverage('66','Commissioning','The commissioning team is familiar with sustainability concepts','Q1');
	newLeverage('66','Commissioning','The owner and general contractor are interested in optimizing facility commissioning to improve overall quality and performance of the final product','Q2');
	newLeverage('66','Project Communications','Electronic programs / forms are available and individuals with expertise are available to run them','I2');
	newLeverage('67','Commissioning','The owner and general contractor are interested in optimizing facility commissioning to improve the overall quality and performance of the final product','Q2');
	newLeverage('67','Health, Safety, & Environment','Project involves buildings with human occupants','J6');
	newLeverage('67','Benchmarking','The project team is interested in evaluating and improving sustainability performance','B3');
	newLeverage('68','Commissioning','The owner and general contractors are interested in optimizing facility commissioning to improve the overall quality and performance of the final product','Q2');
	newLeverage('68','Health, Safety, & Environment','Project involves building with human occupants','J6');
	newLeverage('68','Benchmarking','The project team is interested in evaluating and improving sustainability performance','B3');
	newLeverage('69','Health, Safety, & Environment','The project is located in an area with recognized air quality problems','J10');
	newLeverage('69','Health, Safety, & Environment','Project involves buildings with human occupants','J6');
	newLeverage('69','Benchmarking','The project team is interested in evaluating and improving sustainability performance','B3');	
}

function newLeverage(rank, category, conditions, conditionCode){
	var row = Alloy.createModel('Leverage', {
		// id: auto incremented primary key
		LeverageGUID: Ti.Platform.createUUID(),    
		Rank: rank,
		Category: category,
		Conditions: conditions,
		ConditionCode: conditionCode			
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("LeveragingCondition: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("LeveragingCondition Record Saved");};	
}

function newSA(rank, funct, description, summary, envl, socl, econ, link){
	var row = Alloy.createModel('Saprimary', {
	        SaPrimaryGUID: Ti.Platform.createUUID(),
        	Rank: rank,
        	Function: funct,
        	Description: description,
        	Summary: summary,
        	Envl : envl,
        	Socl : socl,
        	Econ : econ,
        	Link : link
	});
	if (Alloy.Globals.verboseLog) {Ti.API.info("Saprimary: " + JSON.stringify(row));};
	row.save();	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Saprimary Record Saved");};
}

function setupSAs(){
	newSA('1','Project Management','Leadership Team Staffing for Sustainable Projects','','++','++','-','SA1.pdf');
	newSA('2','Project Management','Community Social Responsibility Program','','+','++','-','SA2.pdf');
	newSA('3','Project Management','Contractor Sustainability Program and Recognition System','','+','+','N','SA3.pdf');
	newSA('4','Project Management','Sustainability Provisions in Construction Execution Plans','','+','+','+','SA4.pdf');
	newSA('5','Project Management','Sustainability Risk Management','','+','+','+','SA5.pdf');
	newSA('6','Project Management','Stakeholder Engagement Plan','','+','+','N','SA6.pdf');
	newSA('7','Project Management','Site Work Hour Schedule to Reduce Traffic Impacts','','+','+','-','SA7.pdf');
	newSA('8','Project Management','Work Schedule to Reduce Electricity Impacts','','++','N','N','SA8.pdf');
	newSA('9','Project Management','Paperless Communication and Construction Documentation','','+','N','+','SA9.pdf');
	newSA('10','Project Management','Construction Team Sustainability Performance Assessment','','+','+','N','SA10.pdf');
	newSA('11','Contracting','Verification of Sustainability Claims and Ratings','','+','+','N','SA11.pdf');
	newSA('12','Contracting','Sustainability-friendly Project Delivery Methods','','N','+','N','SA12.pdf');
	newSA('13','Contracting','Contractor Prequalification Based on Safety and Sustainability Performance','','+','+','+','SA13.pdf');
	newSA('14','Contracting','Promotion of Local Employment and Skills Development','','N','++','-','SA14.pdf');
	newSA('15','Contracting','Sustainability Change Proposal Clause','','+','+','N','SA15.pdf');
	newSA('16','Field Engineering','Labor-intensive versus Equipment-intensive Approaches','','N','+','+','SA16.pdf');
	newSA('17','Field Engineering','Pre-assembly and Pre-fabrication of Construction Elements','','+','+','+','SA17.pdf');
	newSA('18','Field Engineering','Sequence and Route Planning for Project Transport','','+','+','N','SA18.pdf');
	newSA('19','Field Engineering',"Minimization of Project's Footprint of Disruption",'','++','+','N','SA19.pdf');
	newSA('20','Field Engineering','Sustainable Material Substitutions','','+','N','N','SA20.pdf');
	newSA('21','Field Engineering','Construction Noise/Vibration Abatement and Mitigation','','+','+','-','SA21.pdf');
	newSA('22','Field Engineering','Selective Demolition versus Conventional Demolition','','++','+','+','SA22.pdf');
	newSA('23','Field Engineering','Sustainable Large-scale Earthwork and Grading Operations','','+','+','+','SA23.pdf');
	newSA('24','Field Engineering','Reduction of Dunnage for Equipment Operations','','+','N','N','SA24.pdf');
	newSA('25','Field Engineering','Reusable Shoring, Formwork, and Scaffolding','','+','N','+','SA25.pdf');
	newSA('26','Site Facilities & Operations','Protection of Cultural Artifacts and Endangered Species','','+','++','-','SA26.pdf');
	newSA('27','Site Facilities & Operations','Protection of Trees and Vegetation','','+','+','N','SA27.pdf');
	newSA('28','Site Facilities & Operations','Sustainable Temporary Facilities','','++','+','+','SA28.pdf');
	newSA('29','Site Facilities & Operations','Sustainable Temporary Worker Camps','','+','++','+','SA29.pdf');
	newSA('30','Site Facilities & Operations','Source of Onsite Power','','+','N','N','SA30.pdf');
	newSA('31','Site Facilities & Operations','Site Energy Management','','+','N','+','SA31.pdf');
	newSA('32','Site Facilities & Operations','Energy-autonomous Pre-manufactured Reusable Facilities','','+','N','+','SA32.pdf');
	newSA('33','Site Facilities & Operations','Indoor Air Quality Improvements','','+','+','N','SA33.pdf');
	newSA('34','Site Facilities & Operations','Collection, Remediation, and Reuse of Gray Water and Storm Water','','+','N','-','SA34.pdf');
	newSA('35','Site Facilities & Operations','Environmentally-friendly Dust and Erosion Control','','+','++','-','SA35.pdf');
	newSA('36','Site Facilities & Operations','Construction and Demolition Waste Management','','++','N','N','SA36.pdf');
	newSA('37','Site Facilities & Operations','Collection, Sorting, and Recycling of Construction Wastes','','++','+','N','SA37.pdf');
	newSA('38','Craft Labor Management','Promotion of Local Workforce Preparedness','','N','++','+','SA38.pdf');
	newSA('39','Craft Labor Management','Expatriates versus Local Employment for Global Projects','','N','+','+','SA39.pdf');
	newSA('40','Craft Labor Management','Promote Community Harmony within a Diverse Project Workforce','','N','++','N','SA40.pdf');
	newSA('41','Materials Management','Analysis of Local Materials/Services versus Non-local/Global Alliance','','+','++','N','SA41.pdf');
	newSA('42','Materials Management','Reduction of Packaging Waste','','++','N','N','SA42.pdf');
	newSA('43','Materials Management','Material- and Equipment-handling Strategy','','+','N','+','SA43.pdf');
	newSA('44','Materials Management','Sustainable Consumable Materials Management','','+','N','N','SA44.pdf');
	newSA('45','Materials Management','Minimization of Material Surplus','','+','N','+','SA45.pdf');
	newSA('46','Materials Management','Management of Surplus Materials','','+','+','N','SA46.pdf');
	newSA('47','Construction Equipment Management','Selection and Replacement of Construction Equipment','','++','N','-','SA47.pdf');
	newSA('48','Construction Equipment Management','Right-sizing of Construction Equipment','','+','N','+','SA48.pdf');
	newSA('49','Construction Equipment Management','Use of Full Transport/Equipment Capacity','','+','+','+','SA49.pdf');
	newSA('50','Construction Equipment Management','Reduction in Idling of Construction Equipment','','+','N','N','SA50.pdf');
	newSA('51','Construction Equipment Management','Inspection and Maintenance of Construction Equipment','','+','N','N','SA51.pdf');
	newSA('52','Construction Equipment Management','Tire-cleaning of Roadworthy Vehicles','','+','+','-','SA52.pdf');
	newSA('53','Quality Management, Commissioning, & Handover','Quality Management and Facility Start-up Planning','','+','+','+','SA53.pdf');
	newSA('54','Quality Management, Commissioning, & Handover','Sustainability Lessons Learned','','+','+','N','SA54.pdf');
	newSA('55','Pre-Construction Planning & Design','Greenfield versus Brownfield Site Selection','','++','+','-','SA55.pdf');
	newSA('56','Pre-Construction Planning & Design','Sustainability Improvements to Landscape Design','','+','+','+','SA56.pdf');
	newSA('57','Pre-Construction Planning & Design','Environmentally-friendly Parking Lots and Sidewalks','','+','N','N','SA57.pdf');
	newSA('58','Pre-Construction Planning & Design','Sustainable Design for Refinery Projects','','++','+','+','SA58.pdf');
	newSA('59','Pre-Construction Planning & Design','Design for High Performance Buildings','','++','+','+','SA59.pdf');
	newSA('60','Pre-Construction Planning & Design','Site and Facility Ergonomics','','N','++','+','SA60.pdf');
	newSA('61','Pre-Construction Planning & Design','Design for Disassembly and Decommissioning','','+','+','+','SA61.pdf');
	newSA('62','Pre-Construction Planning & Design','Design Standardization','','+','N','+','SA62.pdf');
	newSA('63','Pre-Construction Planning & Design','Planning for Future Expansions and Upgrades','','+','+','+','SA63.pdf');
	newSA('64','Pre-Construction Planning & Design','Constructability Planning for Sustainable Projects','','+','+','++','SA64.pdf');
	newSA('65','Project Management','In-depth Implementation Analysis for Sustainability Actions','','+','+','+','SA65.pdf');
	newSA('66','Quality Management, Commissioning, & Handover','Enhanced Project Closeout and Handover','','+','+','+','SA66.pdf');
	newSA('67','Post-Construction Operations & Maintenance','Facility Systems Performance Monitoring and Recalibration','','+','+','+','SA67.pdf');
	newSA('68','Post-Construction Operations & Maintenance','Sustainable Renovation of Facility Equipment','','+','+','+','SA68.pdf');
	newSA('69','Post-Construction Operations & Maintenance','Post-Construction Indoor Air Quality Improvements','','+','+','N','SA69.pdf');
}

function newConditions(){
	newCondition("A","1","The project schedule allows time for selective demolition activity.");
	newCondition("A","2","Sufficient resources are available to modify schedules.");
	newCondition("A","3","The project is schedule-critical.");
	newCondition("A","4","The project schedule and budget are flexible.");
	newCondition("B","1","Significant sustainability activities occurred on the project.");
	newCondition("B","2","Sustainability performance and resource data are available.");
	newCondition("B","3","The project team is interested in evaluating and improving sustainability performance.");
	newCondition("C","1","The building HVAC systems are installed and operational early in construction.");
	newCondition("C","2","Project execution involves large-scale earthwork and grading operations.");
	newCondition("C","3","Project fabrication and/or construction processes involve advanced technologies.");
	newCondition("C","4","The project site includes existing trees and vegetation to be protected.");
	newCondition("C","5","Selection of construction methods involves many complex tradeoffs.");
	newCondition("C","6","Site congestion could result in damage to existing trees/vegetation.");
	newCondition("C","7","The project involves a significant amount of demolition.");
	newCondition("C","8","The project is large and complex.");
	newCondition("C","9","The project is currently in feasibility or early design phase.");
	newCondition("C","10","The project scope includes the construction of new refinery units and support infrastructure.");
	newCondition("D","1","Local solar conditions are conducive to operation of facility solar-support systems.");
	newCondition("D","2","The project is located in an area where water is scarce.");
	newCondition("D","3","The project region has a significant archeological history.");
	newCondition("D","4","The project region has some endangered species.");
	newCondition("D","5","The project site is large in size.");
	newCondition("D","6","The project will generate a significant amount of transport traffic.");
	newCondition("D","7","The project is in a non-arctic or non-desert environment.");
	newCondition("D","8","The project is located in an area with significant traffic congestion.");
	newCondition("D","9","The project site is small in size or congested.");
	newCondition("D","10","There is sufficient infrastructure to minimize traffic into and out of the site.");
	newCondition("E","1","Community members have access to the Internet.");
	newCondition("E","2","Project stakeholders and local community leaders are clearly defined and accessible.");
	newCondition("E","3","The project owner, stakeholders, and/or local community have diverse interests relative to sustainability.");
	newCondition("F","1","Alternative project delivery methods are available for the project.");
	newCondition("F","2","The project contract provides an incentive for contractor-proposed cost-reducing changes (such as a Value Engineering Change clause).");
	newCondition("F","3","The project contract provides some flexibility for contractor material substitutions.");
	newCondition("F","4","The construction contract is the cost-reimbursable type.");
	newCondition("F","5","The project team is interested in including, or has already incorporated, sustainability requirements in the prime contract.");
	newCondition("G","1","Current methods of packaging and using packing materials result in excess waste.");
	newCondition("G","2","International alliance-type sourcing is the contractor’s standard (default) approach.");
	newCondition("G","3","Many material items are purchased in large bulk quantities.");
	newCondition("G","4","The owner and/or contractor have/has large market share and can influence the shipping methods of vendors/suppliers.");
	newCondition("G","5","The project is for a national company that specify goals for local content.");
	newCondition("G","6","The project uses a significant amount of consumable materials to support construction processes.");
	newCondition("G","7","The project involves many small, uncoordinated deliveries.");
	newCondition("G","8","The project owner is not attentive to contractor procurement approaches.");
	newCondition("G","9","The project has a few large vendors and suppliers.");
	newCondition("G","10","The project has a significant number of suppliers and vendors who have or could obtain certifications.");
	newCondition("G","11","The project has mature suppliers and vendors.");
	newCondition("G","12","The project has high local content requirements for materials and services.");
	newCondition("G","13","The project team is interested in improving the accuracy of quantity takeoff estimates.");
	newCondition("G","14","Transport equipment tends to be oversized relative to needs.");
	newCondition("G","15","Work that can be identified to go to SBEs/WBEs/MBEs does not require specialized expertise that might not be locally available.");
	newCondition("H","1","The organization is collaborative and communicative.");
	newCondition("H","2","Contractors and subcontractors have equipment maintenance programs in place.");
	newCondition("H","3","Project management has taken a lead role in endorsing sustainable solutions.");
	newCondition("H","4","The project organization and/or the sustainability program are large in size, scope, and/or effort.");
	newCondition("H","5","The team’s sustainability effort is new, fledgling, or ill-structured.");
	newCondition("H","6","The contractor has experience implementing sustainable solutions/practices.");
	newCondition("H","7","The owner and contractor agree to share the benefits/savings from employing sustainable solutions.");
	newCondition("H","8","The project team has experience incorporating sustainability provisions.");
	newCondition("I","1","All parties are willing to use electronic communications and align on the same electronic systems.");
	newCondition("I","2","Electronic programs/forms are available, and individuals with expertise are available to run them.");
	newCondition("I","3","All project parties have computers or tablets, and knowledge of electronic systems.");
	newCondition("J","1","Adjacent project neighbors are sensitive to project-generated noise, dust, and/or equipment exhaust.");
	newCondition("J","2","Construction activities will cause a significant amount of noise and/or vibration for a lengthy duration of time.");
	newCondition("J","3","A local jurisdiction requires clean-up of any materials placed on roadway.");
	newCondition("J","4","Many community residents live adjacent to the project site.");
	newCondition("J","5","Many neighbors adjacent to the jobsite are sensitive to the noise and/or vibration caused by the project.");
	newCondition("J","6","The project involves buildings for human occupants.");
	newCondition("J","7","Regional labor safety performance is below expectations.");
	newCondition("J","8","The local community, especially project neighbors, are not supportive of the project.");
	newCondition("J","9","The project involves a significant amount of pile-driving, rock hammering, or blasting.");
	newCondition("J","10","The project is located in an area with recognized air quality problems.");
	newCondition("J","11","The project is located in an environmentally/socially sensitive area.");
	newCondition("K","1","The project has a complex logistics program.");
	newCondition("L","1","A stick-built approach would involve a significant amount of scaffolding.");
	newCondition("L","2","Future projects by this contractor will likely entail shoring, formwork, and/or scaffolding.");
	newCondition("L","3","Investment into reusable modular autonomous facility units may be spread over several projects.");
	newCondition("L","4","The project design entails a substantial amount of repetition or modularity, thereby leveraging standardization of shoring, formwork, and/or scaffolding.");
	newCondition("L","5","The project involves a substantial amount of shoring, formwork, and/or scaffolding.");
	newCondition("L","6","The project involves a worker camp.");
	newCondition("L","7","Such units are locally available for rent/lease.");
	newCondition("M","1","An energy management system has not yet been implemented on the project.");
	newCondition("M","2","Project execution requires a significant amount of electrical power.");
	newCondition("M","3","The project is very distant from the local power grid.");
	newCondition("M","4","Regional energy costs are high and/or local grid-sourced power has significant negative environmental impacts.");
	newCondition("M","5","There are several options for providing electricity.");
	newCondition("M","6","Workplace culture has not yet focused on energy efficiency.");
	newCondition("N","1","A local recycling infrastructure is in place.");
	newCondition("N","2","Local regulatory restrictions enable the use of gray water.");
	newCondition("N","3","Manufactured goods that are rejected products can have alternative uses.");
	newCondition("N","4","Other projects in the region can benefit from the reuse of waste from this project.");
	newCondition("N","5","Project construction processes can generate a significant amount of gray water.");
	newCondition("N","6","Project waste management efforts have been minimal.");
	newCondition("N","7","Materials recycling is already part of the project team’s culture.");
	newCondition("N","8","Regional landfill dumping fees are relatively high.");
	newCondition("O","1","The local labor supply is extremely limited.");
	newCondition("O","2","Many capital projects in the area compete for skilled labor.");
	newCondition("O","3","The project size is very large relative to the local labor supply.");
	newCondition("O","4","The project will draw from a migrant labor pool.");
	newCondition("O","5","A sufficient number of contractors are available.");
	newCondition("O","6","The project is suffering from low field craft productivity.");
	newCondition("O","7","The project region offers competitive sources for goods and services.");
	newCondition("O","8","The project will have a culturally diverse workforce.");
	newCondition("O","9","The region has a shortage of skilled craft labor.");
	newCondition("O","10","The tradeoffs associated with the different sources of employment are not obvious.");
	newCondition("P","1","Alternative construction equipment is available in sizes sufficient to support construction activities.");
	newCondition("P","2","Construction equipment capacity is often much higher than needed for the task.");
	newCondition("P","3","Construction equipment fleet managers and operators are mostly unaware of environmental effect of equipment operations.");
	newCondition("P","4","Construction equipment selection decisions are most often driven by convenient availability.");
	newCondition("P","5","The contractor’s existing equipment fleet includes many old pieces that are not fuel-efficient.");
	newCondition("P","6","The project involves a substantial amount of dunnage for temporary support of construction equipment (e.g., cranes).");
	newCondition("P","7","The project involves a substantial amount of heavy construction equipment.");
	newCondition("P","8","The project involves extensive use of immobile heavy cranes.");
	newCondition("Q","1","The commissioning team is familiar with sustainability concepts.");
	newCondition("Q","2","The owner and general contractor are interested in optimizing facility commissioning to improve the overall quality and performance of the final product.");	
}

var db = {

setupDB: function(version){
	//createSettingsTable();
	var version = getCurrentVersion();
	if (Alloy.Globals.verboseLog) { Ti.API.info("Database version: " + version + " Application version: " + Alloy.Globals.DBVersion); };
	if (version == Alloy.Globals.DBVersion){
		if (Alloy.Globals.verboseLog) { Ti.API.info("Database is up to date!"); };
	} else {
		dropAllTables();
		setupSections(false);
		setupImpactScores();
		setupConditionScores();
		setupLeverages();
		setupSAs();
		saveDBVersion(Alloy.Globals.DBVersion);
	}
},

updateProjectRecord: function(record,_ProjGuid)
{
	var now = moment().format('YYYY MM DD hh:mm:ss');
	if (Alloy.Globals.verboseLog) { Ti.API.info("_ProjGuid: " + _ProjGuid); };
	if (_ProjGuid==""){
		// it's new record
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Creating new record: "); };
		_ProjGuid = Ti.Platform.createUUID();
		var project = Alloy.createModel(collectionName, {
			ProjGUID: _ProjGuid, 
			ProjectName:record[1],
			ClientName:record[2],
			ContractorName:record[3],
			ProjectID:record[4],
			ClientID:record[5],
			ContractorID:record[6],
			Description:record[7],
			EvaluatorName:record[8],
			EvaluationDate:record[9],
			ProjectAffiliation:record[10],
			PctComplete:record[11],
			EMail:record[12],
			Telephone:record[13],
			Contributor1:record[14],
			Contributor2:record[15],
			Contributor3:record[16],
			Contributor4:record[17],
			ModifiedBy: "HSE",
			ModifiedDate: now    //USE ISO FORMAT
		});
		project.save();	
		if (Alloy.Globals.verboseLog) {Ti.API.info("New Record Saved");};
	} else {
		if (Alloy.Globals.verboseLog) { Ti.API.info("sb: Updating existing record: "); };
		var projects =  Alloy.createCollection(collectionName);
		projects.fetch();
		projects = projects.where({ProjGUID:_ProjGuid})[0];
		if (Alloy.Globals.verboseLog) { Ti.API.info("Post data:" + JSON.stringify(projects)); };
		//if (Alloy.Globals.verboseLog) { Ti.API.info("GUID:" + JSON.stringify(_ProjGuid)); };
		projects.save({
					ProjectName:record[1],
					ClientName:record[2],
					ContractorName:record[3],
					ProjectID:record[4],
					ClientID:record[5],
					ContractorID:record[6],
					Description:record[7],
					EvaluatorName:record[8],
					EvaluationDate:record[9],
					ProjectAffiliation:record[10],
					PctComplete:record[11],
					EMail:record[12],
					Telephone:record[13],
					Contributor1:record[14],
					Contributor2:record[15],
					Contributor3:record[16],
					Contributor4:record[17],
					ModifiedBy: "HSE",
	        		ModifiedDate: now    //USE ISO FORMAT
				});
		if (Alloy.Globals.verboseLog) {Ti.API.info("Record Saved: " + _ProjGuid);};
	}
	return _ProjGuid;
	//posts.fetch();
	//posts = posts.where({ProjGUID:_ProjGuid});
	//if (Alloy.Globals.verboseLog) { Ti.API.info("Post data after save:" + JSON.stringify(posts)); };	
},

getProjectResults: function(ProjGuid, byMyRank) {
	
	var dbObj = Ti.Database.open(Alloy.Globals.DatabaseName);
	if (byMyRank) {
		var rows = dbObj.execute("SELECT * FROM Saresults WHERE ProjGUID=? order by MyRank", ProjGuid);
	} else {
		var rows = dbObj.execute("SELECT * FROM Saresults WHERE ProjGUID=? order by RISort, SASort", ProjGuid);
	}
	var data = [];
	var i = 0 ;
	var rank = 0;
	
	while (rows.isValidRow())
	{
		rank = i + 1;
		if (byMyRank) {
			data[i]={ResultsGUID:rows.fieldByName('ResultsGUID'),MyRank:rows.fieldByName('MyRank'),Rank:rows.fieldByName('Rank'),SA:rows.fieldByName('SA'),Title:rows.fieldByName('Title'), Link:rows.fieldByName('Link'), saRI:rows.fieldByName('RI')};
			if (Alloy.Globals.verboseLog) {Ti.API.info("By My Rank - Result get: MyRank: " + rows.fieldByName('MyRank') + " Rank: " + rows.fieldByName('Rank') + " SA: " + rows.fieldByName('SA') +  " ResultGUID: " + rows.fieldByName('ResultsGUID'));};
		} else {
			data[i]={ResultsGUID:rows.fieldByName('ResultsGUID'),MyRank:rank,Rank:rank,SA:rows.fieldByName('SA'),Title:rows.fieldByName('Title'), Link:rows.fieldByName('Link'), saRI:rows.fieldByName('RI')};
			if (Alloy.Globals.verboseLog) {Ti.API.info("By RI - Result get: SARank: " + rows.fieldByName('SA') + " MyRank: " + rank + " ResultGUID: " + rows.fieldByName('ResultsGUID'));};
		}
  		rows.next();
  		i++;

	}
	
	Ti.API.info("DB: select results end");
	dbObj.close();
	
	return data;		
},

updateProjectResults: function(ProjGuid, data)
{

	//if (Alloy.Globals.verboseLog) { Ti.API.info("Saving results"); };
	deleteProjectResults(ProjGuid);
	for (var i = 0, len = data.length; i < len; i++){

		var ResultsGUID = Ti.Platform.createUUID();
		var results = Alloy.createModel('Saresults', {
			ResultsGUID: ResultsGUID, 
			ProjGUID:ProjGuid,
			MyRank:1 * data[i].MyRank,
			Rank:1 * data[i].Rank,
			SA:data[i].SA,
			SASort:1 * data[i].SA,
			Title:data[i].Title,
			Link:data[i].Link,
			RI:data[i].saRI,
			RISort:(-100 * data[i].saRI)
			//data[i]={MyRank:title.Rank,Rank:title.Rank,SA:title.Rank,Title:title.Description, Envl:title.Envl, Socl:title.Socl, Econ:title.Econ, Link:title.Link, RI:saRI};
		});
		results.save();	
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Result Saved MyRank: " + data[i].MyRank + " ResultGUID: " + ResultsGUID);};
		//MyRank:tempdata[i].myrank,Rank:tempdata[i].rank,SA:tempdata[i].sa,Title:tempdata[i].title, Link:tempdata[i].link, saRI:tempdata[i].saRI
		if (Alloy.Globals.verboseLog) {Ti.API.info("Result Saved - MyRank: " + data[i].MyRank + " Rank: " + data[i].Rank + " SA: " + data[i].SA + " Title: " + data[i].Title + " Link: " + data[i].Link + " saRI: " + data[i].saRI);};
	}	
},

updateProjectMyRank: function(projGuid, result1, myrank1, rank1, result2, myrank2, rank2){

	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Updating myrank records"); };
	var results =  Alloy.createCollection('Saresults');
	results.fetch();
	results = results.where({ResultsGUID:result1})[0];

	if (Alloy.Globals.verboseLog) { Ti.API.info("1 row results: " + JSON.stringify(results)); };
	if (Alloy.Globals.verboseLog) { Ti.API.info("1 row update for GUID: " + result1 + " myrank = " + myrank1 + " rank = " + rank1); };

	results.save({
				MyRank:myrank1,
				Rank:rank1
			});

	var results = Alloy.createCollection("Saresults");		
	results.fetch();
	results = results.where({ResultsGUID:result2})[0];

	if (Alloy.Globals.verboseLog) { Ti.API.info("2 row results: " + JSON.stringify(results)); };
	if (Alloy.Globals.verboseLog) { Ti.API.info("2 row update for GUID: " + result2 + " myrank = " + myrank2 + " rank = " + rank2); };

	results.save({
				MyRank:myrank2,
				Rank:rank2
			});
	
	updateMyRankFlag(projGuid, true);							
	if (Alloy.Globals.verboseLog) {Ti.API.info("MyRank Records Saved");};	
},

saveMyRank: function(projGuid, tempdata){
	var len= tempdata.length;
	deleteProjectResults(projGuid);
	for (var i = 0; i < len ; i++) {
		//if (Alloy.Globals.verboseLog) {Ti.API.info("SaveMyRank - MyRank: " + tempdata[i].myrank + " Rank: " + tempdata[i].rank + " SA: " + tempdata[i].sa + " Title: " + tempdata[i].descr + " Link: " + tempdata[i].link + " saRI: " + tempdata[i].saRI + " GUID: " + tempdata[i].resultsGUID);};
		var ResultsGUID = Ti.Platform.createUUID();
		var results = Alloy.createModel('Saresults', {
			ResultsGUID: ResultsGUID, 
			ProjGUID:projGuid,
			MyRank:tempdata[i].myrank,
			Rank:tempdata[i].rank,
			SA:tempdata[i].sa,
			SASort:1 * tempdata[i].sa,
			Title:tempdata[i].descr,
			Link:tempdata[i].link,
			RI:tempdata[i].saRI,
			RISort:(-100 * tempdata[i].saRI)
			//data[i]={MyRank:title.Rank,Rank:title.Rank,SA:title.Rank,Title:title.Description, Envl:title.Envl, Socl:title.Socl, Econ:title.Econ, Link:title.Link, RI:saRI};
		});
		results.save();		
	}
	updateMyRankFlag(projGuid, true);
},
		
updateProjectPct: function(record,_ProjGuid){
	var now = moment().format('YYYY MM DD hh:mm:ss');
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Updating existing record objectives pct: "); };
			var projects =  Alloy.createCollection(collectionName);
			projects.fetch();
			projects = projects.where({ProjGUID:_ProjGuid})[0];
			if (Alloy.Globals.verboseLog) { Ti.API.info("Post data:" + JSON.stringify(projects)); };
			projects.save({
						EnvStewardship:record[1],
						SocialProgress:record[2],
						CalcValid:"NO",
						ModifiedBy: "HSE",
		        		ModifiedDate: now    //USE ISO FORMAT
					});
			if (Alloy.Globals.verboseLog) {Ti.API.info("Record Saved: " + _ProjGuid);};	
},

getProjectCondtions: function(_ProjGuid) {
	var projects = Alloy.createCollection(collectionName);
	
	projects.fetch();
	projects = projects.where({ProjGUID:_ProjGuid})[0];
	projects = projects.toJSON();
	//var condt = "";
	//condt = projects.Conditions;
	if (projects.Conditions){
		return projects.Conditions;	
	} else {
		return "";
	}
},

checkProjectCalcsFlag: function(_ProjGuid) {
	var projects = Alloy.createCollection(collectionName);
	
	projects.fetch();
	projects = projects.where({ProjGUID:_ProjGuid})[0];
	projects = projects.toJSON();
	if (projects.CalcValid=='YES'){
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Results are calculated"); };	
		return true;
	} else {
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Results needs to be calculated"); };
		return false;
	}
},

setProjectCalcsFlag: function(_ProjGuid, flag) {
	var now = moment().format('YYYY MM DD hh:mm:ss');
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Updating calculation status"); };
			var projects =  Alloy.createCollection(collectionName);
			projects.fetch();
			projects = projects.where({ProjGUID:_ProjGuid})[0];
			if (flag){
				var status='YES';	
			} else {
				var status='NO';
			}			
			projects.save({
						CalcValid:status,
						ModifiedBy: "HSE",
		        		ModifiedDate: now    //USE ISO FORMAT
					});
			// reset MyRank flag
			if (flag) {updateMyRankFlag(_ProjGuid, false);}		
			if (Alloy.Globals.verboseLog) {Ti.API.info("Record Saved: " + _ProjGuid);};	
},

checkMyRankFlag: function(ProjGuid) {
	var projects = Alloy.createCollection(collectionName);
	
	projects.fetch();
	projects = projects.where({ProjGUID:ProjGuid})[0];
	projects = projects.toJSON();
	if (projects.UseMyRank=='YES'){
		return true;	
	} else {
		return false;
	}
},

setMyRankFlag: function(ProjGuid, flag) {
	updateMyRankFlag(ProjGuid, flag);
},


getProjectPct: function(_ProjGuid) {
	var projects = Alloy.createCollection(collectionName);
	var procents = [];
	
	projects.fetch();
	projects = projects.where({ProjGUID:_ProjGuid})[0];
	projects = projects.toJSON();
	var envlPct = projects.EnvStewardship;
	var soclPct = projects.SocialProgress;
	if (!(envlPct)){
		var envlPct = 0;
	}
	if (!(soclPct)){
		var soclPct = 0;
	}
	procents = [{EnvlPct:envlPct,SoclPct:soclPct}];
	return procents;
},

getLeverageCodes: function(rank) {
	var leverages = Alloy.createCollection('Leverage');
	var codes = [];
	
	leverages.fetch();
	leverages = leverages.where({Rank:rank});
	
	for (var i = 0, len = leverages.length; i < len; i++){
		var leverage = leverages[i].toJSON();
		codes[i] = {Code:leverage.ConditionCode};
	}		
	return codes;
},

getImpactScore: function() {
	var impacts = Alloy.createCollection('Impactscore');
	
	impacts.fetch();
	impacts = impacts.toJSON();
	return impacts;	
},

getConditionScore: function() {
	var conds = Alloy.createCollection('Conditionsscore');
	
	conds.fetch();
	conds = conds.toJSON();
	return conds;	
},

updateProjectConditions: function(conditions,_ProjGuid){
	var now = moment().format('YYYY MM DD hh:mm:ss');
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Updating existing record conditions: "); };
			var projects =  Alloy.createCollection(collectionName);
			projects.fetch();
			projects = projects.where({ProjGUID:_ProjGuid})[0];
			if (Alloy.Globals.verboseLog) { Ti.API.info("Post data:" + JSON.stringify(projects)); };
			projects.save({
						Conditions:conditions,
						CalcValid:"NO",
						ModifiedBy: "HSE",
		        		ModifiedDate: now    //USE ISO FORMAT
					});
			if (Alloy.Globals.verboseLog) {Ti.API.info("Record Saved: " + _ProjGuid);};	
},

createSections: function(delAll) {
	(delAll);
	newConditions();
},

copyProject: function(projGuid, projectName, projectID){
	if (Alloy.Globals.verboseLog) { Ti.API.info("db: Coping projectGuid: " + projGuid + ", New Name: " + projectName + ", New ID: " + projectID); };
	
	var existingProject = Alloy.createCollection(collectionName);
	
	existingProject.fetch();
	existingProject = existingProject.where({ProjGUID:projGuid})[0];
	existingProject = existingProject.toJSON();
	if (Alloy.Globals.verboseLog) {Ti.API.info("Existing project:" + existingProject.ProjectName); };
	if (existingProject.ProjectName.length>0){
			var now = moment().format('YYYY MM DD hh:mm:ss');
			var newGuid = Ti.Platform.createUUID();
			var project = Alloy.createModel(collectionName, {
			ProjGUID: newGuid, 
			ProjectName:projectName,
			ClientName:existingProject.ClientName,
			ContractorName:existingProject.ContractorName,
			ProjectID:projectID,
			ClientID:existingProject.ClientID,
			ContractorID:existingProject.ContractorID,
			Description:existingProject.Description,
			EvaluatorName:existingProject.EvaluatorName,
			EvaluationDate:existingProject.EvaluationDate,
			ProjectAffiliation:existingProject.ProjectAffiliation,
			PctComplete:existingProject.PctComplete,
			EMail:existingProject.EMail,
			Telephone:existingProject.Telephone,
        	EnvStewardship:existingProject.EnvStewardship,        	        	       
            SocialProgress:existingProject.SocialProgress,
            Conditions:existingProject.Conditions,			
			Contributor1:existingProject.Contributor1,
			Contributor2:existingProject.Contributor2,
			Contributor3:existingProject.Contributor3,
			Contributor4:existingProject.Contributor4,
			//Conditions:existingProject.Conditions,
			ModifiedBy: "HSE",
			ModifiedDate: now    //USE ISO FORMAT
		});
		project.save();	
		if (Alloy.Globals.verboseLog) {Ti.API.info("New Record Saved"); };
		return newGuid;
	} else {
		return '';
	}
},
deleteProject: function(projGuid)
	{  //use with caution
		if (Alloy.Globals.verboseLog) { Ti.API.info("db: Deleting Project" + projGuid); };
		if (projGuid.length > 0) {
			var collection =  Alloy.createCollection(collectionName);	
			collection.fetch();
			var sql = "DELETE FROM " + collection.config.adapter.collection_name + " where ProjGUID='" + projGuid + "'";
			db = Ti.Database.open(collection.config.adapter.db_name);
			db.execute(sql);
			sql = "DELETE FROM Saresults WHERE ProjGUID='" + projGuid + "'";
			db.execute(sql);
			db.close();
			collection.trigger('sync');
			return true;
		} else {
			return false;
		}
	},
	
deleteRecord: function (in_guid)
	{
		Ti.API.info("Entering Deleting Record: " + in_guid);			
		var collection =  Alloy.createCollection(collectionName);	
		collection.fetch({ProjGUID:in_guid});
		_.each(_.clone(collection.models), function(model) {
			var record = model.toJSON();
			if (record.ObservationGuid  === in_guid) {		
  				model.destroy();
  				Ti.API.info("Project deleted!: " + in_guid);
  				return true;
  			}
		});	   
		return false;			  							
	}
		
};

module.exports = db;