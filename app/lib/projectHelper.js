
var collectionName = 'Projects';
var moment = require('alloy/moment');
if (Alloy.Globals.verboseLog) { Ti.API.info("projectHelper entry "); };

var observationHelper = {

createTestRecord: function()
{
	
	var now = moment().format('YYYY MM DD hh:mm:ss');
	var stamp = moment().format('ssSSS');
	var record = Alloy.createModel(collectionName, {
		// id: auto incremented primary key
		ProjGUID: Ti.Platform.createUUID(),    
		ProjectName: "Project_" + stamp,
        ProjectID: stamp,
        ClientName: "Client_" + stamp,
        ClientID: stamp,
        ContractorName: "Contractor_" + stamp,
        ContractorID: stamp,
        Description: "1TEXT1",
        EvaluatorName: "1TEXT1",
        ProjectAffiliation: "1TEXT1",   //O (Owner) or C (Contractor)
        EMail: "hse@fluor.com",
        EvaluationDate: "10-Apr-2016", //USE ISO FORMAT
        PctComplete: "1TEXT1",
        Telephone: "1TEXT1",
        Contributor1: "1TEXT1",
        Contributor2: "1TEXT1",
        Contributor3: "1TEXT1",
        Contributor4: "1TEXT1",
        EnvStewardship: "1TEXT1",        	        	       
        SocialProgress: "1TEXT1",
        ObservationDescription: "1TEXT1",
        QA1: "1TEXT1",    //Switch to boolean??
        QA2: "1TEXT1",
        QA3: "1TEXT1",
        QA4: "1TEXT1",
        Conditions: "",
        ModifiedBy: "mor10186",
        ModifiedDate: now    //USE ISO FORMAT				
	});
	Ti.API.info("Record: " + JSON.stringify(record));
	record.save();	
	Ti.API.info("Test Record Saved");
},

showAllRecords: function()
	{
		var posts =  Alloy.createCollection(collectionName);
	//	posts.on("change reset add remove", function(e) { Ti.API.error("Observation Collectoin Callback: " + e); });
	
		posts.fetch();	
		Ti.API.info("Num Records:" + posts.length);		
		posts.forEach(function(post, i) {
			var index = i + 1;				
			Ti.API.info("Record: " + index + ":" + JSON.stringify(post));
	});
	
		
	},

dropTables: function() {
  		var db = Ti.Database.open(Alloy.Globals.DatabaseName);
		db.execute("DROP TABLE IF EXISTS Projects");
		db.close();
		if (Alloy.Globals.verboseLog) { Ti.API.info("Table Projects dopped!"); };
},
	
deleteAllRecords: function()
	{  //use with caution
		if (Alloy.Globals.verboseLog) { Ti.API.info("projectHelper: Deleting All Records"); };
		var collection =  Alloy.createCollection(collectionName);	
		collection.fetch();
		Ti.API.info("Deleting: " + collection.length);			
		var sql = "DELETE FROM " + collection.config.adapter.collection_name;
		db = Ti.Database.open(collection.config.adapter.db_name);
		db.execute(sql);
		db.close();
		collection.trigger('sync');
	}
};

module.exports = observationHelper;