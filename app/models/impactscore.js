exports.definition = {
    config: {
        "columns": {
        	"ScoreGUID": "TEXT NOT NULL PRIMARY KEY",
        	"Symbol": "TEXT",
        	"BaseValue": "TEXT",
        	"MinValue": "TEXT",
        	"MaxValue": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'ScoreGUID',
            "collection_name": "Impactscore"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};