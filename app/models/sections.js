exports.definition = {
    config: {
        "columns": {
        	"SectionGUID": "TEXT NOT NULL PRIMARY KEY",
        	"SectionName": "TEXT",
        	"SectionID": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'SectionGUID',
            "collection_name": "Sections"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};