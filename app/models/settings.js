exports.definition = {
    config: {
        "columns": {
        	"Item": "TEXT NOT NULL",
        	"Value": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
            "db_name": Alloy.Globals.DatabaseName,
            //"idAttribute":'Item',
            "collection_name": "Settings"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};