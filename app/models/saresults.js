exports.definition = {
    config: {
        "columns": {
        	"ResultsGUID": "TEXT NOT NULL PRIMARY KEY",
        	"ProjGUID": "TEXT",
        	"MyRank": "INTEGER",
        	"Rank": "INTEGER",
        	"SA": "TEXT",
        	"SASort": "INTEGER",
        	"Title": "TEXT",
        	"Link":"TEXT",
        	"RI" : "TEXT",
        	"RISort" : "INTEGER"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'ResultsGUID',
            "collection_name": "Saresults"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};