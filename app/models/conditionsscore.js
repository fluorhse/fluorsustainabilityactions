exports.definition = {
    config: {
        "columns": {
        	"CondScoreGUID": "TEXT NOT NULL PRIMARY KEY",
        	"Count": "TEXT",
        	"Score": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'CondScoreGUID',
            "collection_name": "Conditionsscore"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};