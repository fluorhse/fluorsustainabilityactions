exports.definition = {
    config: {
        "columns": {
        	"LeverageGUID": "TEXT NOT NULL PRIMARY KEY",
        	"Rank": "TEXT",
        	"Category": "TEXT",
        	"Conditions": "TEXT",
        	"ConditionCode": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'LeverageGUID',
            "collection_name": "Leverage"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};