exports.definition = {
    config: {
        "columns": {
        	"SaPrimaryGUID": "TEXT NOT NULL PRIMARY KEY",
        	"Rank": "TEXT",
        	"Function": "TEXT",
        	"Description": "TEXT",
        	"Summary": "TEXT",
        	"Envl" : "TEXT",
        	"Socl" : "TEXT",
        	"Econ" : "TEXT",
        	"Link" : "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'SaPrimaryGUID',
            "collection_name": "Saprimary"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};