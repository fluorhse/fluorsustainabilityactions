exports.definition = {
    config: {
        "columns": {
        	"ProjGUID": "TEXT NOT NULL PRIMARY KEY",
        	"ProjectName": "TEXT",
        	"ProjectID": "TEXT",
        	"ClientName": "TEXT",
        	"ClientID": "TEXT",
        	"ContractorName": "TEXT",
        	"ContractorID": "TEXT",
        	"Description": "TEXT",
        	"EvaluatorName": "TEXT",
        	"ProjectAffiliation": "TEXT",   //O (Owner) or C (Contractor)
        	"EMail": "TEXT",
            "EvaluationDate": "TEXT", //USE ISO FORMAT
        	"PctComplete": "TEXT",
        	"Telephone": "TEXT",
        	"Contributor1": "TEXT",
        	"Contributor2": "TEXT",
        	"Contributor3": "TEXT",
        	"Contributor4": "TEXT",
        	"EnvStewardship": "TEXT",        	        	       
            "SocialProgress": "TEXT",
            "ObservationDescription": "TEXT",
            "QA1": "TEXT",    //Switch to boolean??
            "QA2": "TEXT",
            "QA3": "TEXT",
            "QA4": "TEXT",
            "Conditions": "TEXT",
            "CalcValid":"TEXT",
            "UseMyRank":"TEXT",
            "ModifiedBy": "TEXT",
            "ModifiedDate": "TEXT"    //USE ISO FORMAT
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'ProjGuid',
            "collection_name": "Projects"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};