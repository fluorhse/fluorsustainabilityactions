exports.definition = {
    config: {
        "columns": {
        	"ConditionGUID": "TEXT NOT NULL PRIMARY KEY",
        	"ConditionName": "TEXT",
        	"ConditionID": "TEXT",
        	"SectionID": "TEXT"
        },
        "defaults": {
        },
        "adapter": {
            "type": "sql",
             "db_name": Alloy.Globals.DatabaseName,
             "idAttribute":'ConditionGUID',
            "collection_name": "Conditions"
        }
    },
    
     extendModel: function(Model) {
        _.extend(Model.prototype, {
            // extended functions and properties go here                       
        });
 
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
    
    
};