var args = arguments[0] || {};

var projGuid = args.ProjGuid;

var moment = require('alloy/moment');
var projs = require("db");

var evalDate = moment().format('YYYY-MM-DD');

var collectionName = "Projects";

var record = [];

// new project
record[0] = '';

var EnvStewardshipPct = "0";
var SocialProgressPct = "0";
var validProject = true;
var existingProjName = "";
	
$.project.addEventListener('focus',function(e){
	// if (projGuid) {	
		// getProject(projGuid);
	// }
});

$.btnOwner.addEventListener('click',function(e){
	if (this.value== false) {
		turnOff($.btnContractor);
		turnOn($.btnOwner);
	} else{
		turnOff($.btnOwner);
	}
});

$.btnContractor.addEventListener('click',function(e){
	if (this.value== false) {
		turnOff($.btnOwner);
		turnOn($.btnContractor);
	} else{
		turnOff($.btnContractor);
	}
});

$.txtEvaluationDate.addEventListener('click',function(e) {
	var view = Ti.UI.createView({
		bottom: 0
	});
	var picker = Ti.UI.createPicker({
		id: 'pickerEvalDate',
		type: Ti.UI.PICKER_TYPE_DATE,
		//useSpinner for Android only
		useSpinner:true,
		minDate : new Date(2015, 1, 1),
		maxDate : new Date(2099,12,31),
		top : 70,
		borderColor: Alloy.Globals.FluorBlue
	});
	var set = Ti.UI.createButton({
		id: 'buttonEvalDate',
		title : '    Set    ',
		top : 240,
		borderColor: Alloy.Globals.FluorBlue,
		backgroundColor: "#0073d4",
		color:"#ffffff",
		borderWidth: 2,
		borderRadius: 6,
	});

	var month=new Array();
	month[0] = "Jan";
	month[1] = "Feb";
	month[2] = "Mar";
	month[3] = "Apr";
	month[4] = "May";
	month[5] = "Jun";
	month[6] = "Jul";
	month[7] = "Aug";
	month[8] = "Sep";
	month[9] = "Oct";
	month[10] = "Nov";
	month[11] = "Dec";
	
	view.addEventListener("click", function(e){
		picker.hide();
		$.project.remove(view);
	});
	
	set.addEventListener('click',function(e) {
		var strDate = "";
		strDate = picker.value.getDate() + "-" + month[picker.value.getMonth()] + "-" + picker.value.getFullYear();
		$.txtEvaluationDate.setValue(strDate);
		$.project.remove(picker);
		$.project.remove(set);
	});
	
	view.add(picker);
	view.add(set);
	$.project.add(view);
});

if (OS_ANDROID) {
	$.txtConstructionCompleted.addEventListener("keypressed",function(e) {
		//alert("key pressed");
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtConstructionCompleted.setSelection(e.source.value.length, e.source.value.length);
	});
	//for Android, the * and # keys do not trigger a keyprssed event, so let's clean on a blur as well
	$.txtConstructionCompleted.addEventListener("blur",function(e) {
		//alert("blur");
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtConstructionCompleted.setSelection(e.source.value.length, e.source.value.length);
		if ($.txtConstructionCompleted.value > 100) {
			alert("Construction Completed percentage cannot be greater than 100");
		}
	});
} else {
	$.txtConstructionCompleted.addEventListener("change",function(e) {
		//alert("changed");
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtConstructionCompleted.setSelection(e.source.value.length, e.source.value.length);
	});
	
}

$.txtEmail.addEventListener('blur',function(e){
	if ($.txtEmail.value.length) {
		if (!checkemail($.txtEmail.value)) {
			alert("The Email address you have entered is not in the correct format");
		}		
	}
});

function turnOn(e) {
		e.backgroundColor = "#00c000";
		e.title="\u2713";
		e.value = true;	
}
function turnOff(e) {
		e.backgroundColor = "#fff";
		e.title="";
		e.value = false;	
}

function getProject(projGuid){

	record[0] = projGuid;
	if (Alloy.Globals.verboseLog) {Ti.API.info("Review Project where ProjGuid:" + projGuid);};
	
	var projects = Alloy.createCollection(collectionName);
	
	projects.fetch();
	projects = projects.where({ProjGUID:projGuid})[0];
	projects = projects.toJSON();
	
	$.txtProjectName.value = projects.ProjectName;
	existingProjName = projects.ProjectName;
	$.txtProjectClientName.value = projects.ClientName;
	$.txtProjectGeneralContractor.value = projects.ContractorName;
	$.txtProjectID.value = projects.ProjectID;
	$.txtClientID.value = projects.ClientID;
	$.txtContractorID.value = projects.ContractorID;
	$.txtProjDescription.value = projects.Description;
	$.txtEvaluatorName.value = projects.EvaluatorName;
	$.txtEvaluationDate.value = projects.EvaluationDate;
	if (projects.ProjectAffiliation=='O'){
//		$.txtProjectAffiliation.value = "Owner";
		turnOn($.btnOwner);
	} else {
		if (projects.ProjectAffiliation=='C') {
			turnOn($.btnContractor);	
		}
		//$.txtProjectAffiliation.value = "Contractor";
	}
	$.txtConstructionCompleted.value = projects.PctComplete;
	$.txtEmail.value = projects.EMail;
	$.txtTelephone.value = projects.Telephone;
	$.txtContributor1.value = projects.Contributor1;
	$.txtContributor2.value = projects.Contributor2;
	$.txtContributor3.value = projects.Contributor3;
	$.txtContributor4.value = projects.Contributor4;
	EnvStewardshipPct = projects.EnvStewardship;
	SocialProgressPct = projects.SocialProgress;
	projChange();
	
}


function checkemail(emailAddress) {
        var str = emailAddress;
        var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (filter.test(str)) {
            testresults = true;
        } else {
            testresults = false;
        }
        return (testresults);
};

// date format dd-Mmm-yyyy
function checkdate(date) {
        var str = date;
        var filter = /^((([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4})|(T\+[0-9]+)$/;
        if (filter.test(str)) {
            testresults = true;
        } else {
            testresults = false;
        }
        return (testresults);
};

function checkConstructionCompleted(pct) {
        var str = pct;
        var filter = /^[0-9]*$/;
        if (filter.test(str)) {
            testresults = true;
        } else {
            testresults = false;
        }
        //addtional check to ensure number, if entered, is less than 100
        if (pct.length) {
        	if (pct > 100) {
        		testresults = false;
        	}
        }
        return (testresults);
};

function checkProjectDuplicate(value, type){
	var projects = Alloy.createCollection("Projects");
	
	projects.fetch();
	if (type=='NAME'){
		projects = projects.where({ProjectName:value});
	} else {
		projects = projects.where({ProjectID:value});
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("Record count: " + projects.length); };
	return projects.length;
}

function validateProject(){
	var valid = false;
	if (($.txtProjectName.value.length) && ($.txtProjectID.value.length) && ($.txtEvaluatorName.value.length) && ($.txtEmail.value.length) && ($.txtTelephone.value.length)) {
		valid = true;
		if (!checkemail($.txtEmail.value)) {
			valid = false;
		}
		if (!checkdate($.txtEvaluationDate.value)) {
			valid = false;
		}	
		if (!checkConstructionCompleted($.txtConstructionCompleted.value)) {
			valid = false;			
		}		
	}

	return valid;
}

function projChange(e) {
	if (e && e.source.id=='txtProjectName' && existingProjName != $.txtProjectName.value) {
		var count = checkProjectDuplicate($.txtProjectName.value, "NAME");
		if (count>0){
			if (Alloy.Globals.verboseLog) { Ti.API.info("validProject is now false"); };
			validProject = false;
		} else {
			validProject = true;
		}
	}
	if (validateProject() && validProject) {
		$.btnNext.enabled = true;
		$.btnNext.setBackgroundColor("#0073d4");
		$.btnNext.setColor("#ffffff");
	} else {
		$.btnNext.enabled = false;
		$.btnNext.setBackgroundColor("#808080");
		$.btnNext.setColor("#cccccc");
	}
}

function projNameChange(){
	if (!validProject) {
		alert('Project: ' + $.txtProjectName.value + ' already exists! Please select different project name.');
	}		
}

function saveProject(e) {
	if (Alloy.Globals.verboseLog) { Ti.API.info("Saving data - start. Save triggered by: " + e.source.id); };
	
	if (e.source.id=='txtEvaluationDate') {
		for (len = $.project.children.length - 1, i = len; i >=0 ; i--) {
			if ($.project.children[i].id) {
			if ($.project.children[i].id == 'pickerEvalDate' || $.project.children[i].id == 'buttonEvalDate') {
				$.project.remove($.project.children[i]);
			}
			}
		} 
	}
	if (e.source.id == 'txtEvaluationDate' && ($.txtEvaluatorName.value.length) && (!checkdate($.txtEvaluationDate.value))){
		var dialog = Ti.UI.createAlertDialog({
	    	buttonNames: ['Ok'],
	    	message: 'Evaluation date must be in dd-Mmm-yyyy format. For example: 23-Apr-2016',
	    	title: 'Invalid Date Format'
	  	});
	
	  	dialog.show();		
	} 
	if (e.source.id == 'txtEmail' && ($.txtEmail.value.length) && (!checkemail($.txtEmail.value))){
		var dialog = Ti.UI.createAlertDialog({
	    	buttonNames: ['Ok'],
	    	message: 'Email address is not in a valid format.',
	    	title: 'Incorrect Email!'
	  	});
	
	  	dialog.show();		
	}
	if (e.source.id == 'txtConstructionCompleted' && ($.txtConstructionCompleted.value.length) && (!checkConstructionCompleted($.txtConstructionCompleted.value))){
		var dialog = Ti.UI.createAlertDialog({
	    	buttonNames: ['Ok'],
	    	message: 'Construction Completed must be a numerical value between 1-100.',
	    	title: 'Incorrect Construction Completed!'
	  	});
	
	  	dialog.show();		
	}
	record[1] = $.txtProjectName.value;
	record[2] = $.txtProjectClientName.value;
	record[3] = $.txtProjectGeneralContractor.value;
	record[4] = $.txtProjectID.value;
	record[5] = $.txtClientID.value;
	record[6] = $.txtContractorID.value;
	record[7] = $.txtProjDescription.value;
	record[8] = $.txtEvaluatorName.value;
	record[9] = $.txtEvaluationDate.value;
	
	if ($.btnOwner.value == true) { 
		record[10] = "O";		
	} else {
		if($.btnContractor.value == true) {
			record[10] = "C";
		}
	}
	record[11] = $.txtConstructionCompleted.value;
	record[12] = $.txtEmail.value;
	record[13] = $.txtTelephone.value;
	record[14] = $.txtContributor1.value;
	record[15] = $.txtContributor2.value;
	record[16] = $.txtContributor3.value;
	record[17] = $.txtContributor4.value;
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("Record data:" + JSON.stringify(record)); };

	if ((record[1].length != 0) && (record[9].length != 0)) {
		projGuid = projs.updateProjectRecord(record, record[0]);
		record[0] = projGuid;
		if (Alloy.Globals.verboseLog) { Ti.API.info("Data saved for ProjId: " + record[0]); };
	};

	if (Alloy.Globals.verboseLog) { Ti.API.info("Saving data - end"); };
}

function nextClick(e){
	Ti.API.info(JSON.stringify("Next screen for ProjGuid: " + projGuid));
	if (validProject) {
		saveProject(e);
	} else {
		if (validateProject()) {alert('Project: ' + $.txtProjectName.value + ' already exists! Please select different project name.');}
		return false;
	}
	Alloy.Globals.Navigator.open('objectives',{ProjGuid:projGuid});
	closeWindow();	
}

function backClick(e){
	Ti.API.info(JSON.stringify("Back screen for ProjGuid: " + projGuid));
	if (validProject) {
		saveProject(e);
		closeWindow();
	} else {
		if (validateProject()) {
			alert('Project: ' + $.txtProjectName.value + ' already exists! Please select different project name.');
		} else {
			closeWindow();
		}
	}
}
function closeWindow(){
	$.project.close();
}

// Code
if (Alloy.Globals.verboseLog) { Ti.API.info("args.ProjGuid: " + args.ProjGuid); };

$.btnNext.enabled = false;

if (projGuid) {	
	getProject(projGuid);
}
