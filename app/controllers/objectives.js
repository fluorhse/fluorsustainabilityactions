//=======================  DECLARATIONS  ============================
var args = arguments[0] || {};

var projs = require("db");

var projGUID = args.ProjGuid;

var LANDSCAPE=0;
var PORTRAIT=1;

var record = [];
var bEdited = false;
var bSuppressChange=false;

//=======================  FUNCTIONS  ============================
$.objectiveInfo.addEventListener("scroll", function(e){
	if(Ti.Platform.osname == 'android'){
			Ti.UI.Android.hideSoftKeyboard();
	} else {
			$.txtEnvironmental.blur();
			$.txtSocial.blur();
	}	
});

 function hintClick(e){
	 var hintId = e.source.id;
	 var txtMsg = "";
	 if (Alloy.Globals.verboseLog) { Ti.API.info("Hint click: " + hintId); };
	 switch (hintId) {
		 case 'imgQM1' : {
			 txtMsg = 'Criteria air pollutants, Energy consumption, Greenhouse gases, Indoor air quality, Land use, Light pollution, Noise, Odors, Waste generation, Water consumption, Water quality'; 
			 break;			
		 }
		 case 'imgQM2': {
			 txtMsg = 'Community infrastructure, Communication relationships, Community service/donations, Health and safety, Jobs created, Local resource depletion, Skills development, Tax revenue produced, Traffic'; 
			 break;
		 }
		 case 'imgQM3': {
			 txtMsg = 'Project fiscal impacts'; 
			 break;		
		 }				
	 }
	 var dialog = Ti.UI.createAlertDialog({
    	 message: txtMsg,
    	 ok: 'Close',
    	 title: 'Most Affected Areas/Resources'
  	 });
  	 dialog.show();
 }

function isNumber(num) {
    return !isNaN(+num);
}

//clear out default value (0) upon entry
$.txtSocial.addEventListener('focus',function(e){
	if (!bSuppressChange) {
		if (Alloy.Globals.verboseLog) { Ti.API.info("In txtSocial focus event"); };
		if (e.value=="0") {
			$.txtSocial.value="";
		}		
	}
});
$.txtEnvironmental.addEventListener('focus',function(e){
	if (!bSuppressChange) {
		if (Alloy.Globals.verboseLog) { Ti.API.info("In txtEnvironmental focus event"); };
			if (e.value=="0") {
			$.txtEnvironmental.value="";
		}		
	}
});

if (OS_ANDROID) {
	$.txtEnvironmental.addEventListener("keypressed",function(e) {
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtEnvironmental.setSelection(e.source.value.length, e.source.value.length);
		projChange();
	});
	$.txtSocial.addEventListener("keypressed",function(e) {
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtSocial.setSelection(e.source.value.length, e.source.value.length);
		projChange();
	});
	$.txtEnvironmental.addEventListener("blur",function(e) {
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtEnvironmental.setSelection(e.source.value.length, e.source.value.length);
		if (e.source.value=="") {
			e.source.value=0;
		}
		projChange();
		saveProject();
	});
	$.txtSocial.addEventListener("blur",function(e) {
		e.source.value = e.source.value.replace(/[^0-9]+/, "");
		$.txtSocial.setSelection(e.source.value.length, e.source.value.length);
		if (e.source.value=="") {
			e.source.value=0;
		}
		projChange();
		saveProject();
	});

} else {  //for iOS we need to look for change event, instead of keypressed
	  $.txtEnvironmental.addEventListener("change",function(e) {
			 if (Alloy.Globals.verboseLog) { Ti.API.info("keypressed - txtEnvironmental"); };
			 if (Alloy.Globals.verboseLog) { Ti.API.info("bSuppressChange = " + bSuppressChange); };
	 		 if (!bSuppressChange) {
			 if (Alloy.Globals.verboseLog) { Ti.API.info("In txtEnvironmental change event"); };
			 	 e.source.value = e.source.value.replace(/[^0-9]+/, "");
			 	 $.txtEnvironmental.setSelection(e.source.value.length, e.source.value.length);
				 projChange();	 			
	 		 }
	  });
	  $.txtSocial.addEventListener("change",function(e) {
	 		 if (!bSuppressChange) {
			 if (Alloy.Globals.verboseLog) { Ti.API.info("In txtSocial change event"); };
			 if (Alloy.Globals.verboseLog) { Ti.API.info("bSuppressChange = " + bSuppressChange); };
			 	 e.source.value = e.source.value.replace(/[^0-9]+/, "");
			 	 $.txtSocial.setSelection(e.source.value.length, e.source.value.length);
				 projChange();	 			
	 		 }
	  });
 }



function projChange() {
	if (Alloy.Globals.verboseLog) { Ti.API.info("In projChange function"); };
	var bPctOk = true;
	if ((isNumber($.txtEnvironmental.value)) && (isNumber($.txtSocial.value))){
	} else {
		bPctOk = false;
	}
	if ((100-$.txtSocial.value-$.txtEnvironmental.value) < 0) {
		bPctOk = false;
	}
	if (bPctOk){
		$.lblEconomicPct.text=100-$.txtEnvironmental.value-$.txtSocial.value;
		//in case they didn't remove the zero before typing the number'
		//if ($.txtEnvironmental.value > 0) {
		//	$.txtEnvironmental.value = parseInt($.txtEnvironmental.value);
		//}
		//if ($.txtSocial.value > 0) {
		//	$.txtSocial.value = parseInt($.txtSocial.value);
		//}
		$.btnNext.enabled = true;
		$.btnNext.setBackgroundColor("#0073d4");
		$.btnNext.setColor("#ffffff");
		bEdited = true;
	} else {
		$.lblEconomicPct.text="Error!";
		$.btnNext.enabled = false;
		$.btnNext.setBackgroundColor("#808080");
		$.btnNext.setColor("#cccccc");
	}
	return;
}

function saveProject() {
	 if (Alloy.Globals.verboseLog) { Ti.API.info("Saving pct data - start"); };
	 if (bEdited) {
		 if (Alloy.Globals.verboseLog) { Ti.API.info("In saveProject fucntion"); };
		 if ((isNumber($.txtEnvironmental.value)) && (isNumber($.txtSocial.value)) && (isNumber($.lblEconomicPct.text))){
			 if (($.txtEnvironmental.value!=args.EnvStewardship)||($.txtSocial.value!=args.SocialProgress)){
				 if ($.txtEnvironmental.value==="") {
					 record[1] = "0";					
				 } else {
					 record[1] = $.txtEnvironmental.value;				
				 }
				 if ($.txtSocial.value==="") {
					 record[2] = "0";					
				 } else {
					 record[2] = $.txtSocial.value;				
				 }
				 projs.updateProjectPct(record, record[0]);
				 if (Alloy.Globals.verboseLog) { Ti.API.info("Data saved for ProjId: " + record[0]); };
			 }
		 }
	 }
	if (Alloy.Globals.verboseLog) { Ti.API.info("Saving data - end"); };
}



// listen for orientation changes
Ti.Gesture.addEventListener('orientationchange', function(e) {
		//alert("orientation change detected");
    	console.log("Orientation changed to: " + e.orientation);
    	console.log(e);
    // here you react to the change in orientation
    if (OS_IOS) {
	    	if (e.source.orientation === Ti.UI.LANDSCAPE_LEFT || e.source.orientation === Ti.UI.LANDSCAPE_RIGHT){
	    		updateUI(LANDSCAPE);
	    	} else {  //have to specifically check for other orientation, so it's not triggered with a laying flat gesture
	    		if (e.source.orientation === Ti.UI.PORTRAIT || e.source.orientation === Ti.UI.UPSIDE_PORTRAIT){
	    			updateUI(PORTRAIT);
	    		}
	    	}
	} else {
    	if (e.source.orientation==Ti.UI.PORTRAIT) {
    		updateUI(PORTRAIT);
    			console.log("Update UI to PORTRAIT for Android");
    	} else {
    		if (e.source.orientation==Ti.UI.LANDSCAPE_LEFT) {
    			updateUI(LANDSCAPE);
    			console.log("Update UI to LANDSCAPE for Android");    			
    		}
    	}
    }
});

function updateUI(isPortrait){
    // here you update the UI based on the current orientation
    if (isPortrait){
        console.log('Adjusting for portrait');
        $.ImgWhenHorz.visible="false";
		$.ImgWhenHorz.width="0";
        $.ImgWhenVert.visible="true";
		$.ImgWhenVert.height="33%";
        $.ContentLeftView.width="99%";
        //$.PercentageWrapper.height="25%";
        //$.QuestionWrapper.height="25%";
    }else{
        console.log('Adjusting for landscape');
        $.ImgWhenVert.visible="false";
		$.ImgWhenVert.height="0";
        $.ImgWhenHorz.visible="true";
		$.ImgWhenHorz.width="49%";
        $.ContentLeftView.width="50%";
        //$.PercentageWrapper.height="49%";
        //$.QuestionWrapper.height="49%";
    }
}

function nextClick(){
	$.activityIndicator.show();
	Ti.API.info(JSON.stringify("Next screen for ProjGuid: " + projGUID));
	saveProject();
	Alloy.Globals.Navigator.open('set_conditions',{ProjGuid:projGUID});
	$.objective.close();	
}

function backClick(){
	$.activityIndicator.show();
	Ti.API.info(JSON.stringify("Back screen for ProjGuid: " + projGUID));
	saveProject();
	Alloy.Globals.Navigator.open('edit_project',{ProjGuid:projGUID, direction: 'back'});
	closeWindow();
}
function closeWindow(){
	$.objective.close();
}

//=========================  CODE START  ============================

//  NOTE:  get the orientation by screen dimensions, in case device has not yet been rotated, etc.
var pWidth = Ti.Platform.displayCaps.platformWidth;
var pHeight = Ti.Platform.displayCaps.platformHeight;
if (pWidth > pHeight) {
	var oriStart=LANDSCAPE;
} else {
	var oriStart=PORTRAIT;	
}
console.log("set initial orientation as portrait:" + oriStart);
if (oriStart===PORTRAIT) {
	console.log("setting initial orinetation as portrait");
 	updateUI(PORTRAIT);
} else {
    console.log("setting Android initial orientation as landscape");
    updateUI(LANDSCAPE);
}
			 
var percents = db.getProjectPct(projGUID);

if (Alloy.Globals.verboseLog) { Ti.API.info("In Code Start block step 1: bSuppressChange = " + bSuppressChange); };
bSuppressChange = true;
if (Alloy.Globals.verboseLog) { Ti.API.info("In Code Start block step 2: bSuppressChange = " + bSuppressChange); };

if (isNumber(percents[0].SoclPct)){
	$.txtSocial.value=percents[0].SoclPct;
}
if (isNumber(percents[0].EnvlPct)){
	$.txtEnvironmental.value=percents[0].EnvlPct;
}
if (Alloy.Globals.verboseLog) { Ti.API.info("In Code Start block step 3: bSuppressChange = " + bSuppressChange); };
bSuppressChange = false;
if (Alloy.Globals.verboseLog) { Ti.API.info("In Code Start block step 4: bSuppressChange = " + bSuppressChange); };

if ((isNumber($.txtEnvironmental.value)) && (isNumber($.txtSocial.value))){
		$.lblEconomicPct.text=100-$.txtSocial.value-$.txtEnvironmental.value;
		$.btnNext.enabled = true;
		$.btnNext.setBackgroundColor("#0073d4");
		$.btnNext.setColor("#ffffff");
}

record[0]=projGUID;

$.objective.addEventListener("open",function() {
	$.txtEnvironmental.focus();
});
