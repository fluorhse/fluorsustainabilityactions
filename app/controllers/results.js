var args = arguments[0] || {};

db = require('db');

var projGuid = args.ProjGuid;
var record = [];
var lastrow = 0;
var tabledata = [];

var calcsStatus = db.checkProjectCalcsFlag(projGuid);
var collectionName = "Projects";

function getImpactScores(){
	var impact = [];
	impacts = db.getImpactScore();	
	//if (Alloy.Globals.verboseLog) {Ti.API.info("impacts: " + JSON.stringify(impacts));};
	return impacts;
}

function resetRank(){
	var dialog = Ti.UI.createAlertDialog({
    	cancel: 1,
    	buttonNames: ['Confirm', 'Cancel'],
    	message: 'Are you sure you want to reset the rank order of the pdf files for this project back to their default values?',
    	title: 'Reset Ranking'
  	});
  	dialog.addEventListener('click', function(e){
    	if (e.index === e.source.cancel){
      		if (Alloy.Globals.verboseLog) {Ti.API.info('The cancel button was clicked'); };
    	} else {
			db.setMyRankFlag(projGuid, false);
			tabledata = [];
			$.titlesTable.data = tabledata;
			createTitleViews();	
		}
  	});
  	dialog.show();
}

function getImpact(impacts, symbol){
	var value = '';
	if (impacts.length){
		
		for (var i = 0, len = impacts.length; i < len; i++){
				if(symbol === impacts[i].Symbol){
					value = impacts[i].BaseValue;
					//if (Alloy.Globals.verboseLog) {Ti.API.info("impacts value: " + value);};
					break;
				}		
		}		
	}
	return value;
}

function getCondtsScores(){
	var condt = [];
	condt = db.getConditionScore();	
	//if (Alloy.Globals.verboseLog) {Ti.API.info("impacts: " + JSON.stringify(impacts));};
	return condt;
}

function getCondtsScore(scores, count){
	var score = 0;	
	for (var i = 0, len = scores.length; i < len; i++){
			if(count == 1 * scores[i].Count){
				score = scores[i].Score;
				//if (Alloy.Globals.verboseLog) {Ti.API.info("impacts value: " + value);};
				break;
			}		
	}		
	return score;
}

function getSATitles(){
	var data = [];
	
	if (!calcsStatus) {
		if (Alloy.Globals.verboseLog) {Ti.API.info("Results needs to be calculated!");};
		var percents = db.getProjectPct(projGuid);
		var conditions = db.getProjectCondtions(projGuid);
		var condts = getCondtsScores();
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Proj pcts: " + JSON.stringify(percents));};
		
		var envlPct = 1.0 * percents[0].EnvlPct;
		var soclPct = 1.0 * percents[0].SoclPct;
		var econPct = 100 - envlPct - soclPct;
		
		if (envlPct>0){
			envlPct = envlPct/100;
		}
		if (soclPct>0){
			soclPct = soclPct/100;
		}
		if (econPct>0){
			econPct = econPct/100;
		}
		
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Reading action titles.");};
	
		var titles = Alloy.createCollection('Saprimary');
		
		var value = '';
		var impacts = [];
		impacts = getImpactScores();
		
		titles.fetch();
		titles = titles.toJSON();
		
		for (var i = 0, len = titles.length; i < len; i++){
				title = titles[i];
				var impactEnvl = 1.0 * getImpact(impacts, title.Envl);
				var impactSocl = 1.0 * getImpact(impacts, title.Socl);
				var impactEcon = 1.0 * getImpact(impacts, title.Econ);
				impactEnvl = impactEnvl * envlPct;
				impactSocl = impactSocl * soclPct;
				impactEcon = impactEcon * econPct;
				var saRI = impactEnvl+impactSocl+impactEcon;
				//saRI = saRI.toString();
				var leverageCodes = db.getLeverageCodes(title.Rank);
				//if (Alloy.Globals.verboseLog) {Ti.API.info("SA Rank: " + title.Rank + " leverage codes = " + JSON.stringify(leverageCodes));};
				var condCount = 0;
				if (conditions.length>0){
					for (j=0, lev = leverageCodes.length; j < lev; j++){
						//if (Alloy.Globals.verboseLog) {Ti.API.info("leverage codes = " + leverageCodes[j].Code);};
						var pos = conditions.indexOf("|"+leverageCodes[j].Code+"|");
						if (pos>=0) {condCount++;}
					}
				}
				//if (Alloy.Globals.verboseLog) {Ti.API.info("SA#: " + title.Rank + " conditions = " + conditions + " count = " + condCount);};
				var score = getCondtsScore(condts, condCount);
				saRI = saRI * score;
				saRI = parseFloat(saRI).toFixed(2);
				//if (Alloy.Globals.verboseLog) {Ti.API.info("saRI = " + saRI);};
				if (saRI=='-0.00') {saRI='0.00';}
				//var score = getCondtsScore();
				//if (Alloy.Globals.verboseLog) {Ti.API.info("impactEnvl = " + impactEnvl +" impactSocl = " + impactSocl +" impactEcon = " + impactEcon+" saRI = " + saRI);};
				data[i]={MyRank:title.Rank,Rank:title.Rank,SA:title.Rank,Title:title.Description, Link:title.Link, saRI:saRI};	
				
		
		}
		// save ranking data
		db.updateProjectResults(projGuid, data);
		db.setProjectCalcsFlag(projGuid, true);
		
		data = [];
	}		
	// read ranking data to get it sorted by RI, SA 
	var myRankFlag = db.checkMyRankFlag(projGuid);		
	data = db.getProjectResults(projGuid, myRankFlag);
	
	// now rows are sorted by rank so we need to save and read them again to get correct ranks
	if (!calcsStatus) {
		db.updateProjectResults(projGuid, data);
		data = db.getProjectResults(projGuid, myRankFlag);
	}	

	return data;
}


function createTitleViews(){
	var titles = getSATitles();
	
	//if (Alloy.Globals.verboseLog) {Ti.API.info("Conditions count: " + conditions.length);};
	lastrow = titles.length;

	for (var i = 0, len= titles.length; i < len ; i++) {
		
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Create Condition: " + conditions[i].ConditionId + ", name: " + conditions[i].ConditionName);};
		var view = Ti.UI.createView({
   			id:"viewtitle"+titles[i].Rank,
    		backgroundColor:"#ffffff",
    		width: Ti.UI.FILL,
    		layout:"horizontal",
    		height: Ti.UI.SIZE
		});
		//data[i]={ResultsGUID,MyRank,Rank,SA,Title, Link, saRI};
		var viewRow = Ti.UI.createTableViewRow({
   			id:"rowtitle"+titles[i].Rank,
			sa:titles[i].SA,
			descr:titles[i].Title,
			link:titles[i].Link,
			saRI:titles[i].saRI,
			resultsGUID:titles[i].ResultsGUID,
			rank:titles[i].Rank,
			myrank:titles[i].MyRank
		});
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Create view MyRank: " + titles[i].MyRank + " Rank: " + titles[i].Rank + " ResultGUID: " + titles[i].ResultsGUID);};

		var imageviewmyRank1 = $.UI.create('ImageView',{
			id:"ivRank1" + titles[i].Rank,
			//rownumber: titles[i].Rank,
			classes:["imgUpDown"],
			action:'moveUp'
		});
		var labelmyRank = $.UI.create('Label',{
  			id:"lblmyrank" + +titles[i].Rank,
  			text:titles[i].MyRank,
  			classes:["LabelRank"], 
		});
		var imageviewmyRank2 = $.UI.create('ImageView',{
			id:"ivRank2" + titles[i].Rank,
			//rownumber: titles[i].Rank,
			classes:["imgUpDown"],
			//image:'images/arrow_down.png',
			action:'moveDown'
		});
		var labelRank = $.UI.create('Label',{
  			id:"lblrank" + +titles[i].Rank,
  			text:titles[i].Rank,
  			classes:["LabelRank"],
		});

		// for iOS we add the move event, since they can dynamically move rows, 
		// otherwise we give them up/down arrow images
		if (OS_IOS) {
			viewRow.addEventListener('move', function(e) {
				if (Alloy.Globals.verboseLog) {Ti.API.info("Move action fromIndex: " + e.fromIndex + " to index: " + e.index);}
				if (Alloy.Globals.verboseLog) {Ti.API.info("Move action e: " + JSON.stringify(e));};
				updateMyRankonRowMove(1*e.index, 1*e.fromIndex);
			});	
		} else {
			if (i > 0) {
				imageviewmyRank1.image = 'images/arrow_up.png';			
			}
			if (i < len-1) {
				imageviewmyRank2.image = 'images/arrow_down.png';			
			}
		}
				
		var labelTitle = $.UI.create('Label',{
  			id:"lblTitle" + titles[i].Rank,
  			text:'SA' + titles[i].SA + "  " + titles[i].Title,
  			classes:["LabelDesc"],
  			saNumber: titles[i].SA,
  			action:'showPDF'
		});
		
		var labelRankIndex = $.UI.create('Label',{
  			id:"lblindex" + titles[i].Rank,
  			text:titles[i].saRI,
  			classes:["LabelIndex"],
 
		});

		view.add(imageviewmyRank1);
		view.add(labelmyRank);
		view.add(imageviewmyRank2);
		view.add(labelRank);
		view.add(labelTitle);
		view.add(labelRankIndex);
		viewRow.add(view);
		tabledata.push(viewRow);
	};
	$.titlesTable.setData(tabledata);
	//if (Alloy.Globals.verboseLog) {Ti.API.info("Create rows - tabledata: " + tabledata);};
}


$.titlesTable.addEventListener('click', function(evt) {
	//alert("action=" + evt.source.action);
    var action = evt.source.action,
        index = evt.index,
        isFirstRow = index === 0,
        isLastRow = index + 1 === tabledata.length;
    if(action === 'moveUp' && !isFirstRow) {
        //swapRows(index - 1, index);
        updateMyRankonRowMove(index - 1, index);
    }
    else if(action === 'moveDown' && !isLastRow) {
        //swapRows(index, index + 1);
        updateMyRankonRowMove(index, index + 1);
    }
});

$.titlesTable.addEventListener('singletap', function(evt) {
	//alert("action=" + evt.source.action);
    var action = evt.source.action,
        sa = evt.source.saNumber;
	if(action === 'showPDF') {
		var pdfFilename = "SA"+sa+".pdf";
		var pdfFileSpec = "pdf/" + pdfFilename;
		if(OS_IOS){			
			appFile = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory,pdfFileSpec);
			var appfilepath = appFile.nativePath;
			docViewer = Ti.UI.iOS.createDocumentViewer({url:appfilepath});
			docViewer.show();
		} else {
			var originalFile = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory,pdfFileSpec);
			appFile = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory,pdfFilename);
			appFile.write(originalFile.read());
			var appfilepath = appFile.nativePath;
			try {
				Ti.Android.currentActivity.startActivity(Ti.Android.createIntent({
					action: Ti.Android.ACTION_VIEW,
					type: 'application/pdf',
					data: appfilepath
				}));
			} catch(e) {
				Ti.API.info('error trying to launch activity, e =' + e);
				alert("Unable to display " + pdfFilename + ".\n\nThis device does not have a PDF application installed");
			}
		}

	}
});

function updateMyRankonRowMove(index, fromIndex){
	if (Alloy.Globals.verboseLog) {Ti.API.info("Moving - index: " + index + " fromindex: " + fromIndex);};
	var temprowFrom = tabledata[fromIndex];
	var len= tabledata.length;
	if (!OS_IOS) {
		if (index==0){
			temprowFrom.children[0].children[0].image = 'images/no_arrow.png';
			tabledata[index].children[0].children[0].image = 'images/arrow_up.png';	;
		}
		if (fromIndex==len-1){
			temprowFrom.children[0].children[2].image = 'images/arrow_down.png';
			tabledata[index].children[0].children[2].image = 'images/no_arrow.png';	;			
		}
	}	
	var tempdata = [];
	if (index>fromIndex){
		for (var i = 0;  i < len ; i++) {
			if((i>index)||(i<fromIndex)){
				tempdata[i] = tabledata[i];
			}
			if((i>=fromIndex)&&(i<index)){
				tempdata[i] = tabledata[i+1];
				tempdata[i].children[0].children[1].text = i+1;
				tempdata[i].myrank = i+1;				
			}
			if(i==index) {
				tempdata[i] = temprowFrom;
				tempdata[i].children[0].children[1].text = index + 1;
				tempdata[i].myrank = index + 1;
					
			}
		}		
	} else {
		for (var i = 0; i < len ; i++) {
			if((i<index)||(i>fromIndex)){
				tempdata[i] = tabledata[i];
			}
			if((i<=fromIndex)&&(i>index)){
				tempdata[i] = tabledata[i-1];
				tempdata[i].children[0].children[1].text = i + 1;
				tempdata[i].myrank = i + 1;				
			}
			if(i==index) {
				tempdata[i] = temprowFrom;
				tempdata[i].children[0].children[1].text = index + 1;
				tempdata[i].myrank = index + 1;				
			}	
			//savedata[i]={MyRank:tempdata[i].myrank,Rank:tempdata[i].rank,SA:tempdata[i].sa,Title:tempdata[i].descr, Link:tempdata[i].link, saRI:tempdata[i].saRI};
			//if (Alloy.Globals.verboseLog) {Ti.API.info("Up - MyRank: " + tempdata[i].myrank + " Rank: " + tempdata[i].rank + " SA: " + tempdata[i].sa + " Title: " + tempdata[i].descr + " Link: " + tempdata[i].link + " saRI: " + tempdata[i].saRI);};
		}					
	}
		
	//$.titlesTable.data = [];
	$.titlesTable.data = tempdata;
	tabledata = [];
	tabledata = tempdata;
	db.saveMyRank(projGuid, tempdata);
}

function doneClick(e){
	Ti.API.info(JSON.stringify("Done for ProjGuid: " + projGuid));
	$.results.close();
}

function backClick(e){
	$.activityIndicator.show();
	Ti.API.info(JSON.stringify("Back screen for ProjGuid: " + projGuid));
	Alloy.Globals.Navigator.open('set_conditions',{ProjGuid:projGuid, direction: 'back'});
	closeWindow();
}

function sendEmail() {
	//alert("open email dialog here");
	var strSubject = "Fluor SAs Project Summary";
	var strBody="";

	record[0] = projGuid;
	if (Alloy.Globals.verboseLog) {Ti.API.info("Get Project Info for summary where ProjGuid:" + projGuid);};
	
	var projects = Alloy.createCollection(collectionName);
	
	projects.fetch();
	projects = projects.where({ProjGUID:projGuid})[0];
	projects = projects.toJSON();

	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Build email body with summary data");};
	//TODO: Get input values used from database
	strBody = "Following are the Fluor Sustainability Action PDF files, as ranked by myself and the Fluor SAs mobile application:\n\n";
	
	strBody += "Project: " + projects.ProjectName + "\n";
	strBody += "Project ID: " + projects.ProjectID + "\n";
	strBody += "Evaluator Name: " + projects.EvaluatorName + "\n";
	strBody += "Evaluation Date: " + projects.EvaluationDate + "\n";
	strBody += "Email: " + projects.EMail + "\n";
	strBody += "Telephone: " + projects.Telephone + "\n";
	strBody += "Env Stewardship: " + projects.EnvStewardship + "\n";
	strBody += "Social Progress: " + projects.SocialProgress + "\n";
	var strConditions = db.getProjectCondtions(projGuid);  //At some point we should globaly correct spelling of condtions
	//because | is a regular expression special character, we replace all non-alpha numeric with @, so we can use it instead
	strConditions = strConditions.replace(/[^A-Z0-9]/g,"@"); 
	strConditions = strConditions.replace(/@@/g," : ").replace(/@/g,"");
	strBody += "Conditions: " + strConditions + "\n";  

	var rowCount;
	if (Alloy.Globals.verboseLog) {Ti.API.info("loop through ranked rows");};
	for (var i = 0, len= tabledata.length; i < len ; i++) {
		rowCount = i+1;
		strBody += "My Rank:" + rowCount + "      Rank:" + tabledata[i].children[0].children[3].text + "     PDF: SA" + tabledata[i].children[0].children[4].saNumber + "\n";
	}
	if (Alloy.Globals.verboseLog) {Ti.API.info("strBody = " + strBody);};
	
	var emailDialog = Ti.UI.createEmailDialog();
	if (!emailDialog.isSupported()){
		Ti.UI.createAlertDialog({
			title:'Error',
			message:'Email not available/configured on this device'
		}).show();
		return;
	}
	emailDialog.subject = strSubject;
	emailDialog.messageBody = strBody;
	
	emailDialog.addEventListener('complete',function(e) {
		if (e.result == emailDialog.SENT) {
			//Android doesn't give us any useful result codes
			if (!OS_ANDROID) { 
				alert("Your Email has been sent.");
			} 
		} else {
			if (e.result == emailDialog.CANCELLED) {
				if (!OS_ANDROID) { 
					alert("Your Email has been cancelled.");
				} 				
			} else {
				alert("Email was not sent.  Result = " + e.result);			
			}
		}
	});
	
	emailDialog.open();
}

function closeWindow(){
	$.results.close();
}

//=========================  CODE START  ============================

createTitleViews();
