var args = arguments[0] || {};

function showIntroduction() {
	Alloy.Globals.Navigator.open("introduction", {displayHomeAsUp:true});
}

function showUserGuide() {
	Alloy.Globals.Navigator.open("user_guide", {displayHomeAsUp:true});
}

function showAdminScreen() {
	Ti.API.debug("main: showAdminScreen");
	//Alloy.Globals.Navigator.open("admin");
	Alloy.Globals.Navigator.open("admin", {displayHomeAsUp:true});
}
