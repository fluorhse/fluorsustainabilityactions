//=============  DECLARATIONS  ===============
var args = arguments[0] || {};

//==============  FUNCTIONS  =================
function onItemClick(e){
	
}

$.wrapper.addEventListener("focus", function(e) { 	 	
 	Alloy.Collections.projects.fetch();
 	if (Alloy.Collections.projects.length==0){
    	$.projectsTable.visible=false;
 		$.viewNoRecordsFound.visible = true;
 	} else {
 		$.viewNoRecordsFound.visible = false; 		
    	$.projectsTable.visible=true;
 	}
});

function openProject(e)
{	
	$.activityIndicator.show();
	var db = require('db');
	Ti.API.info(JSON.stringify("Updating Record: " + e.source.ProjGuid));
	if (db.checkProjectCalcsFlag(e.source.ProjGuid)){
		Alloy.Globals.Navigator.open('results',{ProjGuid:e.source.ProjGuid});
	} else {
		Alloy.Globals.Navigator.open('edit_project',{ProjGuid:e.source.ProjGuid});
	}
	$.activityIndicator.hide();
}

//Alloy.Collections.projects.fetch();

function initialSortByEvalDate(a,b) {
	//TODO:  At some point, see about consolidating initialSortByFormDate and sortByFormDate
	var x = a.get('EvaluationDate');
	var y = b.get('EvaluationDate');
	return ((x < y) ? 1 : ((x > y) ? -1 : 0));
}

function filterFunction(collection) {
	//var obs = require('db');

    var tmpCollection = collection;
    Ti.API.info("projectList: initial ordering by date");
    var recCount = tmpCollection.length;
    if (recCount==0) {    	
    	$.projectsTable.visible=false;
    	$.lblNoRecordsFound.visible=true;
    	alert("Should be hidden: recCount" + recCount);
    } else {
    	$.lblNoRecordsFound.visible=false;
    	$.projectsTable.visible=true;
    	alert("Should be visible: recCount" + recCount);
    }
    var SortedCollection = tmpCollection.sort(initialSortByEvalDate); 
    Ti.API.info("Collection: " + JSON.stringify(SortedCollection));   	
    return SortedCollection;
    //Ti.API.info("Collection: " + JSON.stringify(tmpCollection));
    //return tmpCollection;
}

function addProject(e){
	
	if (Alloy.Globals.verboseLog) { Ti.API.info("On click: Add project (directory)"); };
	
	Alloy.Globals.Project.unid = 0;
	if (Alloy.Globals.verboseLog) { Ti.API.info("New project - ID: " + Alloy.Globals.Project.unid); };
	
	/**
	 * Get the Item that was clicked
	 */
	//var item = $.listView.sections[e.sectionIndex].items[e.itemIndex];
	
	/**
	 * Open the profile view, and pass in the user data for this contact
	 */
	var data = {unid: Alloy.Globals.Project.unid};
	Alloy.Globals.Navigator.open("edit_project", data);
}

function deleteProject(e){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Delete project: " + e.source.projGuid); };
	 
	var dialog = Ti.UI.createAlertDialog({
    	cancel: 1,
    	projGuid: e.source.projGuid,
    	buttonNames: ['Confirm', 'Cancel'],
    	message: 'Are you sure you want to delete project: ' + e.source.projName + '?',
    	title: 'Delete project'
  	});
  	dialog.addEventListener('click', function(e){
    	if (e.index === e.source.cancel){
      		if (Alloy.Globals.verboseLog) {Ti.API.info('The cancel button was clicked'); };
    	} else {
    		if (Alloy.Globals.verboseLog) {Ti.API.info('Deleting project'); };
    		var db = require('db');
    		var result = db.deleteProject(e.source.projGuid);
			if (result) {
				//alert('Project has been deleted.');
				Alloy.Collections.projects.fetch();
			 	if (Alloy.Collections.projects.length==0){
      				if (Alloy.Globals.verboseLog) {Ti.API.info('Last record deleted; displaying viewNoRecordsFound'); };
			 		$.viewNoRecordsFound.visible=true;
			 		//don't ask me why, but we need to reset top and left to get viewNoRecordsFound to show. otherwise it's way up and to the left off the screen
			 		$.viewNoRecordsFound.left=20;
			 		$.viewNoRecordsFound.top=0;
			    	$.projectsTable.visible=false;
			 	} else {
			 		$.viewNoRecordsFound.visible = false; 		
			    	$.projectsTable.visible=true;
			 	}				
			} else {
				alert('Error! Failed to delete project. Please try again.');
			}    		
    	}

  	});
  	dialog.show();
}

function copyProject(e){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Copy project: " + e.source.projGuid); };
	Alloy.Globals.Navigator.open('copy_project',{ProjGuid:e.source.projGuid,ProjName:e.source.projName,ProjID:e.source.projID});
}

//Ensure menu is displayed
function doOpen(e) {
	if (OS_ANDROID) {
		$.wrapper.activity.invalidateOptionsMenu();
	}
}


function cleanup() {
	$.destroy();
}
//=============  CODE START  ===============

Alloy.Collections.projects.fetch();


