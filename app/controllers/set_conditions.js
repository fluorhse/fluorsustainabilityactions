var args = arguments[0] || {};

var projGuid = args.ProjGuid;

var db = require("db");

var LANDSCAPE=0;
var PORTRAIT=1;

var conditionCheckboxes = readProjectConditions();
if (Alloy.Globals.verboseLog) {Ti.API.info("Conditions state string: " + conditionCheckboxes);};

function getSections(){
	var data = [];
	
	if (Alloy.Globals.verboseLog) {Ti.API.info("Reading sections");};
	
	var sections = Alloy.createCollection('Sections');
	
	sections.fetch();
	sections.forEach(function(section, i) {
			var index = i + 1;				
			//if (Alloy.Globals.verboseLog) {Ti.API.info("Record: " + index + ":" + JSON.stringify(section));};
			section = section.toJSON();
			data[i]={SectionId:section.SectionID,SectionName:section.SectionName};
			//if (Alloy.Globals.verboseLog) {Ti.API.info("Data - id: " + data[i].SectionId + " name: " + data[i].SectionName);};
	});
	
	return data;
}

function getConditions(sectionId){
	var data = [];
	
	//if (Alloy.Globals.verboseLog) {Ti.API.info("Reading conditions for section: " + sectionId);};
	if (sectionId.length) {
		var conditions = Alloy.createCollection('Conditions');
		
		conditions.fetch();
		conditions = conditions.where({SectionID:sectionId});
		conditions.forEach(function(condition, i) {
				var index = i + 1;				
				//if (Alloy.Globals.verboseLog) {Ti.API.info("Record: " + index + ":" + JSON.stringify(condition));};
				condition = condition.toJSON();
				data[i]={ConditionId:condition.ConditionID,SectionId:condition.SectionID, ConditionName:condition.ConditionName};
				//if (Alloy.Globals.verboseLog) {Ti.API.info("Data - id: " + data[i].ConditionId + " name: " + data[i].ConditionName);};
		});
	}
	return data;
}

function readProjectConditions(){
	return db.getProjectCondtions(projGuid);
}

function saveConditions(conditionId, value){
	if (value){
		conditionCheckboxes = conditionCheckboxes + "|" + conditionId + "|";
	}else{
		conditionCheckboxes = conditionCheckboxes.replace("|" + conditionId + "|","");
	}
	db.updateProjectConditions(conditionCheckboxes, projGuid);
	if (Alloy.Globals.verboseLog) {Ti.API.info("Conditions state string: " + conditionCheckboxes);};
}

function createConditionViews(sectionId){
	var conditions = getConditions(sectionId);
	var title = "";
	var val = false;
	var bgcolor = "";
	//if (Alloy.Globals.verboseLog) {Ti.API.info("Conditions count: " + conditions.length);};
	for (var i = 0, len= conditions.length; i < len ; i++) {
		
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Create Condition: " + conditions[i].ConditionId + ", name: " + conditions[i].ConditionName);};
		var view = Ti.UI.createView({
   			id:"viewcond"+conditions[i].SectionId + conditions[i].ConditionId,
    		backgroundColor:"#ffffff",
    		width: Ti.UI.FILL,
    		layout:"horizontal",
    		height: Ti.UI.SIZE
		});
		var pos = conditionCheckboxes.indexOf("|"+conditions[i].SectionId + conditions[i].ConditionId+"|");
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Search Condition: *" + conditionCheckboxes + "*, pos: " + pos + " , search for: " + "|"+conditions[i].SectionId + conditions[i].ConditionId+"|");};
		if (pos < 0){
			title = "";
			val = false;
			bgcolor = "#fff";
		} else {
			title = "\u2713";
			val = true;
			bgcolor = "#00c000";			
		}
		//var button = Ti.UI.createButton({
		var button = $.UI.create('Button',{
				title:title,
  				classes:["Checkbox"],
				backgroundColor: bgcolor,
				conditionId:conditions[i].SectionId + conditions[i].ConditionId,
				id:"button" + conditions[i].SectionId + conditions[i].ConditionId,
				value: val
		});
		//var label = Ti.UI.createLabel({
		var label = $.UI.create('Label',{
  			id:"lblcond" + conditions[i].SectionId + + conditions[i].ConditionId,
  			//text:conditions[i].ConditionId + ". " + conditions[i].ConditionName,
  			text:conditions[i].ConditionName,
  			classes:["qLabel"],
    		//color:Alloy.Globals.FluorBlue,
    		//textAlign: "left",
    		//top: "0",
    		//left: "10",
    		//width:"85%",
    		//height:Ti.UI.SIZE,
			//font:{
			//	fontSize:"20",
			//	fontWeight:"bold"
			//}
		});
		var labelNumber = $.UI.create('Label',{
  			id:"lblNumbercond" + conditions[i].SectionId + + conditions[i].ConditionId,
  			text:conditions[i].ConditionId + ". ",
  			classes:["qLabelNumber"],
    		//color:Alloy.Globals.FluorBlue,
    		//textAlign: "left",
    		//top: "0",
    		//left: "10",
    		//width:"35",
    		//height:Ti.UI.SIZE,
			//font:{
			//	fontSize:"20",
			//	fontWeight:"bold"
			//}
		});
		//label.width='auto';
		//label.width='auto';
		button.addEventListener('click',function(e){
			if (Alloy.Globals.verboseLog) {Ti.API.info("Condition clicked: " + e.source.conditionId);};
			if (e.source.value) {
				e.source.title="";
				e.source.value = false;
				e.source.backgroundColor = "#fff";	
			} else {
				e.source.title="\u2713";
				e.source.value = true;
				e.source.backgroundColor = "#00c000";	
			}
			if (Alloy.Globals.verboseLog) {Ti.API.info("e.source.title: " + e.source.title);};
			if (Alloy.Globals.verboseLog) {Ti.API.info("e.source.value: " + e.source.value);};
			saveConditions(e.source.conditionId,e.source.value);
		});
		view.add(button);
		view.add(labelNumber);
		view.add(label);
		$.projectConditions.add(view);
	};
}

function createViews(){
	var sections = [];
	sections = getSections();
	//if (Alloy.Globals.verboseLog) {Ti.API.info("Sections count: " + sections.length);};
	
	for (var i = 0, len= sections.length; i < len ; i++) {
		
		//if (Alloy.Globals.verboseLog) {Ti.API.info("Create Section: " + sections[i].SectionId + ", name: " + sections[i].SectionName);};
		var separator = Ti.UI.createView({
   			id:"viewseparator"+sections[i].SectionId,
   			color: "#ffffff",
    		backgroundColor:"#ffffff",
    		width: Ti.UI.FILL,
    		height: "10"
		});
		var view = Ti.UI.createView({
   			id:"viewtitle" + sections[i].SectionId,
   			color: "#ffffff",
    		backgroundColor:"#0073d4",
    		font:{
    			fontSize: "12"
    		},
    		width: Ti.UI.FILL,
    		height: Ti.UI.SIZE
		});
		var label = $.UI.create('Label',{
  			id:"lbltitle" + sections[i].SectionId + i,
  			text:sections[i].SectionId + ". " +sections[i].SectionName,
  			classes:["SectionLabel"]
		});
		view.add(label);
		if (i>0) {	//add some caption  space to all but the first section
			$.projectConditions.add(separator);			
		}
		$.projectConditions.add(view);
		$.projectConditions.add(separator);
		createConditionViews(sections[i].SectionId);		
	};
}

function clearAll(){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Clear checkboxes"); };
	 
	var dialog = Ti.UI.createAlertDialog({
    	cancel: 1,
    	buttonNames: ['Confirm', 'Cancel'],
    	message: 'Are you sure you want to clear all conditions?',
    	title: 'Clear conditions'
  	});
  	dialog.addEventListener('click', function(e){
    	if (e.index === e.source.cancel){
      		if (Alloy.Globals.verboseLog) {Ti.API.info('The cancel button was clicked'); };
    	} else {
    		if (Alloy.Globals.verboseLog) {Ti.API.info('Deleting conditions'); };
			clearAllCheckboxex();
    	}

  	});
  	dialog.show();
 }

function clearAllCheckboxex(){
	for (i=0, len = $.projectConditions.children.length; i < len; i++) {
		Ti.API.info(JSON.stringify("Children: " + $.projectConditions.children[i].id));
		var viewID = $.projectConditions.children[i].id;
		if (viewID.indexOf('viewcond')>=0) {
		// if ($.projectConditions.children[i].children[0].id){
			 var buttonID = $.projectConditions.children[i].children[0].id;
			 Ti.API.info(JSON.stringify("ButtonID: " + buttonID));
			if (buttonID.indexOf('button')>=0) {
				$.projectConditions.children[i].children[0].title="";
				$.projectConditions.children[i].children[0].value = false;
				$.projectConditions.children[i].children[0].backgroundColor = "#fff";
			}
		}
	}
	db.updateProjectConditions('', projGuid);
}

function nextClick(){
	//alert("set display indicator");
	$.activityIndicator.show();
	Ti.API.info(JSON.stringify("Next screen for ProjGuid: " + projGuid));
	Alloy.Globals.Navigator.open('results',{ProjGuid:projGuid});
	closeWindow();	
}

function backClick(){
	$.activityIndicator.show();
	Ti.API.info(JSON.stringify("Back screen for ProjGuid: " + projGuid));
	Alloy.Globals.Navigator.open('objectives',{ProjGuid:projGuid, direction: 'back'});
	closeWindow();
}
function closeWindow(){
	$.conditions.close();
}
//=========================  CODE START  ============================

if (OS_IOS){
	$.btnClearAll.visible = true;
}

createViews();

