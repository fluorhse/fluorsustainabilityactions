var args = arguments[0] || {};

var projGuid = args.ProjGuid;
var newProjGuid = '';
var projName = args.ProjName;
var projID = args.ProjID;

$.lblProjectName.text = projName;
$.lblProjectIDText.text = projID;

if (($.txtNewProjectName.value.length) && ($.txtNewProjectID.value.length)) {
	$.btnCopy.enabled = true;
	$.btnCopy.setBackgroundColor("#0073d4");
	$.btnCopy.setColor("#ffffff");
} else {
	$.btnCopy.enabled = false;
	$.btnCopy.setBackgroundColor("#808080");
	$.btnCopy.setColor("#cccccc");	
}

function validateProject(value, type){
	var projects = Alloy.createCollection("Projects");
	
	projects.fetch();
	if (type=='NAME'){
		projects = projects.where({ProjectName:value});
	} else {
		projects = projects.where({ProjectID:value});
	}
	if (Alloy.Globals.verboseLog) { Ti.API.info("Record count: " + projects.length); };
	return projects.length;
}

function projChange(e) {
	// var count = validateProject($.txtNewProjectName.value, "NAME");
	// if (count>0){
		// alert('ProjectID: ' + $.txtNewProjectName.value + 'already exists! Please select different project ID.');
		// return false;
	// }
	// count = validateProject($.txtNewProjectID.value, "ID");
	// if (count>0){
		// alert('ProjectID: ' + $.txtNewProjectName.value + 'already exists! Please select different project ID.');
		// return false;
	// }
	if (($.txtNewProjectName.value.length) && ($.txtNewProjectID.value.length)) {
		$.btnCopy.enabled = true;
		$.btnCopy.setBackgroundColor("#0073d4");
		$.btnCopy.setColor("#ffffff");
	} else {
		$.btnCopy.enabled = false;
		$.btnCopy.setBackgroundColor("#808080");
		$.btnCopy.setColor("#cccccc");	
	}

	return;

}

function saveProject(e) {
	if (Alloy.Globals.verboseLog) { Ti.API.info("Saving data - start"); };
	
	var count = validateProject($.txtNewProjectName.value, "NAME");
	if (count>0){
		alert('Project: ' + $.txtNewProjectName.value + ' already exists! Please select different project name.');
		$.txtNewProjectName.focus();
		return false;
	}
	// count = validateProject($.txtNewProjectID.value, "ID");
	// if (count>0){
		// alert('ProjectID: ' + $.txtNewProjectID.value + ' already exists! Please select different project ID.');
		// $.txtNewProjectID.focus();
		// return false;
	// }
	
	if (($.txtNewProjectName.value.length) && ($.txtNewProjectID.value.length)) {
		var db = require("db");
		var result = db.copyProject(projGuid,$.txtNewProjectName.value ,$.txtNewProjectID.value);
		newProjGuid = result;
		if (result != '') {
			var dialog = Ti.UI.createAlertDialog({
		    	close: 0,
		    	buttonNames: ['Close'],
		    	message: 'New project has been created as a copy of ' + $.lblProjectName.text + ' project.',
		    	title: 'New project'
		  	});
		  	dialog.addEventListener('click', function(e){
		    	if (e.index === e.source.close){
		      		closeWindow();
		    	}  		
		  	});
		  	dialog.show();			
			//alert('New project has been created as a copy of ' + $.lblProjectName.text + ' project.');
			//closeWindow();
		} else {
			alert('Error! Failed to copy project data. Please try again.');
		}
	}

	// record[1] = $.txtProjectName.value;
	// record[2] = $.txtProjectClientName.value;
	// record[3] = $.txtProjectGeneralContractor.value;
	// record[4] = $.txtProjectID.value;
	// record[5] = $.txtClientID.value;
	// record[6] = $.txtContractorID.value;
	// record[7] = $.txtProjDescription.value;
	// record[8] = $.txtEvaluatorName.value;
	// record[9] = $.txtEvaluationDate.value;
	// //if ($.txtProjectAffiliation.value == "Owner"){
	// if ($.btnOwner.value == true) { 
		// record[10] = "O";		
	// } else {
		// if($.btnContractor.value == true) {
			// record[10] = "C";
		// }
	// }
	// record[11] = $.txtConstructionCompleted.value;
	// record[12] = $.txtEmail.value;
	// record[13] = $.txtTelephone.value;
	// record[14] = $.txtContributor1.value;
	// record[15] = $.txtContributor2.value;
	// record[16] = $.txtContributor3.value;
	// record[17] = $.txtContributor4.value;
// 	
	// if (Alloy.Globals.verboseLog) { Ti.API.info("Record data:" + JSON.stringify(record)); };
// 
	// if ((record[1].length != 0) && (record[9].length != 0)) {
		// record[0] = projs.updateProjectRecord(record, record[0]);
		// if (Alloy.Globals.verboseLog) { Ti.API.info("Data saved for ProjId: " + record[0]); };
	// };

	if (Alloy.Globals.verboseLog) { Ti.API.info("Saving data - end"); };
}


function copyClick(){
	Ti.API.info(JSON.stringify("Copy project for ProjGuid: " + projGuid));
	saveProject();	
}

function cancelClick(){
	Ti.API.info(JSON.stringify("Cancel for ProjGuid: " + projGuid));
	closeWindow();
}
function closeWindow(){
	if (newProjGuid != '') {
		Alloy.Globals.Navigator.open('edit_project',{ProjGuid:newProjGuid});
	} 
	$.project.close();
}