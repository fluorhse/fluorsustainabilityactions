var args = arguments[0] || {};

var projs = require("projectHelper");
var db = require("db");

// Create a new post, save it locally
function postSave() {
	projs.createTestRecord();		
}

function deleteAllRecords() {
	projs.deleteAllRecords();
}

function showAllRecords() {
	projs.showAllRecords();
}

function dropAllTables() {
	projs.dropTables();
}

function sectionRecords(){
	if (Alloy.Globals.verboseLog) { Ti.API.info("Create sections - start"); };
	db.createSections(true);
	if (Alloy.Globals.verboseLog) { Ti.API.info("Create sections - end"); };
}
