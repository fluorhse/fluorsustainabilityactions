/**
 * Global Navigation Handler
 */

Alloy.Globals.Navigator = {
	
	/**
	 * Handle to the Navigation Controller
	 */
	navGroup: $.nav,
	
  	
	open: function(controller, payload){
		var win = Alloy.createController(controller, payload || {}).getView();
		
		if(OS_IOS){
			if (Alloy.Globals.verboseLog) { Ti.API.info("payload:" + JSON.stringify(payload)); };
			if (payload.direction == "back" || payload.displayHomeAsUp) {
				if (Alloy.Globals.verboseLog) { Ti.API.info("animation = false"); };
				$.nav.openWindow(win, {animated:false});
			} else {
				$.nav.openWindow(win);
			}	
		}
		else if(OS_MOBILEWEB){
			$.nav.open(win);
		}
		else {
			// Android
			// added this property to the payload to know if the window is a child
			if (payload.displayHomeAsUp){
				
				win.addEventListener('open',function(evt){
					var activity=win.activity;
					activity.actionBar.displayHomeAsUp=payload.displayHomeAsUp;
					activity.actionBar.onHomeIconItemSelected=function(){
						evt.source.close();
					};
				});
			}
			win.open();
		}
	}
};

/** Open appropriate start window **/
if(OS_IOS){
	$.nav.open();
}
else if(OS_MOBILEWEB){
	// TODO: If nav.open works here, delete index.open
	$.nav.open();
}
else{
	// TODO: If nav.open works here, delete index.open
	$.nav.getView().open();
}

