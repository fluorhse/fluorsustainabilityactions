// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.Version="v1.1";
Alloy.Globals.DBVersion="1.0";

// This variable allows us to control different features that are enabled for testing only
// Set to "false" for production release
Alloy.Globals.Development = "false";

Alloy.Globals.deviceWidth=Ti.Platform.displayCaps.platformWidth;
Alloy.Globals.deviceHeight=Ti.Platform.displayCaps.platformHeight;
Alloy.Globals.listViewHeight=Alloy.Globals.deviceHeight-65;
Alloy.Globals.WCWidth=0.5*Alloy.Globals.deviceWidth;
Alloy.Globals.WCHeight=0.1*Alloy.Globals.deviceHeight;
Alloy.Globals.osname=Ti.Platform.osname;

Alloy.Globals.DatabaseName = "HSE_FSA";
Alloy.Globals.FluorBlue="#00467f";

Alloy.Globals.verboseLog = "true";

// Project data objcect
Alloy.Globals.Project = {};
Alloy.Globals.Project.unid = 0;
Alloy.Globals.Project.ProjectName = '';
Alloy.Globals.Project.ClientName = '';
Alloy.Globals.Project.GeneralContractor = '';
Alloy.Globals.Project.ProjectRefId= '';
Alloy.Globals.Project.ClientRefId='';
Alloy.Globals.Project.ContractorRefId='';
Alloy.Globals.Project.ProjectDescription ='';
Alloy.Globals.Project.EvalDate='';
Alloy.Globals.Project.ModifiedDate='';
Alloy.Globals.Project.ModifiedBy='';
Alloy.Globals.Project.Active='Y';

var db = require('db');
db.setupDB();

//function setupDB() {
//	// TODO: After DB is in place/working:  try 'WITHOUT ROWID' at the end of the execute to see if it's more performant
//	if (Alloy.Globals.verboseLog) { Ti.API.info("alloy.js: setupDB - start"); };
//	var db = Ti.Database.open(Alloy.Globals.DatabaseName);
//	
//	db.execute('CREATE TABLE if NOT EXISTS FSAProjects ("ProjID" INTEGER PRIMARY KEY, "ProjName" TEXT, "ClientName" TEXT, "GeneralContractor" TEXT, "ProjectRefID" TEXT, "ClientRefID" TEXT, "ContractorRefID" TEXT, "Description" TEXT, "EvalDate" TEXT, "ModifiedDate" TEXT, "ModifiedBy" TEXT, "Active" TEXT)');


//	db.close();
	
//	if (Alloy.Globals.verboseLog) { Ti.API.info("alloy.js: setupDB - end"); };
//}; 

//setupDB();

//======================  GLOBAL FUNCTIONS  =================



